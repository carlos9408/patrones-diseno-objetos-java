<h1 style="text-align:center;"> <strong>Notas a mano sobre análisis orientado a objetos y patrones de diseño</strong></h1>

----

<h1>Documentación</h1>

<p style="text-align:justify;">
En este proyecto se describe cada uno de los patrones propuestos por el GoF. Cada paquete del proyecto contiene un ejemplo práctico presentado en el libro <a href="https://www.amazon.com/dp/B0CW1JH98V">Notas a mano sobre programación orientada a objetos y patrones de diseño</a>
 escrito por Carlos Orozco
</p>

<p style="text-align:justify;">
<strong>Nota:</strong> Como escritor e ingeniero de software espero que los ejemplos sean de utilidad para cualquiera que se cruce con este proyecto.
</p>

* **Autor**: Phd. MSc. Eng. Astr(e) Carlos Eduardo Orozco
* **Año**: 2024
* **Versión**: 1.0

----

<h1> Instrucciones </h1>

1. Clonar el proyecto
2. Ir al paquete con el patrón de interés (ej: creacional -> singleton)
3. Ejecutar la clase Main.java

----

<h1> Patrones de diseño </h1>

<p style="text-align:justify;">
    Un usuario interesado en consultar patrones específicos puede ir directamente a los patrones de interés:
</p>

<h2>- [Patrones creacionales](src/main/java/com/corozco/libro/patrones/creacional/README.md)</h2>
<h2>- [Patrones estructurales](src/main/java/com/corozco/libro/patrones/estructural/README.md)</h2>
<h2>- [Patrones de comportamiento](src/main/java/com/corozco/libro/patrones/comportamiento/README.md)</h2>