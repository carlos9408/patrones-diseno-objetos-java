package com.corozco.libro.patrones.creacional.builder.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class General {

    private long id;

    private String codigo;

}

