<h1 style="text-align:center;"> <strong>Abstract Factory (Fábrica Abstract)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>AbstractFactory</strong> provee una interfaz que permite crear objetos que pertenecen a una familia o grupo común de clases de forma dinámica. De esta forma, un cliente que solicite crear un objeto solo necesita conocer a la fábrica.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>AbstractFactory</strong> provee una interfaz común para la creación de una o varias familias de productos. Gracias al principio de inversión de dependencias (las abstracciones no deben depender de las implementaciones) es posible crear variantes de un producto de manera transparente través de una interfaz.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

La interfaz <strong>IFabricaAbstracta</strong> expone los métodos para la creación de productos concretos. Cada <strong>
FabricaConcreta</strong> implementa la lógica necesaria para crear instancias de la clase <strong>
ProductoConcreto</strong> que sean solicitados por un cliente a través de la <strong>FabricaConcreta</strong>.

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/creacional/abstract_factory.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Producto:</strong> Instancia solicitada por el cliente.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ProductoConcreto:</strong> Subclase de la clase <strong>Producto</strong>.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>FabricaConcreta:</strong> Clase que sabe cual es el producto concreto que se debe retornar al cliente.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>IFabricaAbstracta:</strong> Contrato que provee los métodos para instanciar un <strong>Producto</strong> que pertenece a una <strong>FabricaConcreta</strong>.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Asegura que los objetos que hacen parte de una familia son compatibles entre sí.</p>
   </li>

   <li>
      <p style="text-align:justify;">Los clientes dependen de interfaces o clases abstractas en lugar de clases concretas, lo que mejora la desacoplamiento y la flexibilidad.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite cambiar fácilmente el conjunto de productos utilizados en una aplicación simplemente cambiando la fábrica concreta.</p>
   </li>

   <li>
      <p style="text-align:justify;">Nuevas variantes de productos pueden ser introducidas con cambios mínimos en el código cliente, simplemente extendiendo las fábricas abstractas.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Introduce varias capas adicionales de abstracción, lo que puede complicar el diseño y aumentar la curva de aprendizaje.</p>
   </li>

   <li>
      <p style="text-align:justify;">Extender una familia de productos puede requerir cambios en la interfaz <strong>AbstractFactory</strong> y todas sus subclases, lo que viola el principio de abierto/cerrado.</p>
   </li>

   <li>
      <p style="text-align:justify;">La necesidad de inicializar y configurar fábricas concretas al inicio puede complicar la gestión de dependencias.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Interfaz de usuario multiplataforma:</strong> En aplicaciones que deben funcionar en múltiples sistemas operativos, donde los componentes de la interfaz de usuario, como ventanas y botones, necesitan ser creados de acuerdo con la plataforma sin que el código cliente conozca los detalles de implementación.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Soporte para múltiples temas de apariencia:</strong> Cuando un software debe ofrecer diferentes temas o estilos visuales, un <strong>AbstractFactory</strong> permite cambiar fácilmente entre familias de objetos que representan distintos temas sin modificar el código cliente.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Gestión de recursos para diferentes regiones:</strong> En aplicaciones que deben adaptarse a diferentes regiones geográficas, con diferentes idiomas y configuraciones regionales, un <strong>AbstractFactory</strong> puede proporcionar la infraestructura necesaria para la localización de recursos y presentaciones.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Pruebas y simulaciones:</strong> Cuando se realizan pruebas automáticas o simulaciones, un <strong>AbstractFactory</strong> puede usarse para crear mock objects o stubs que imitan el comportamiento de los objetos reales, permitiendo el desarrollo y prueba de partes del sistema en aislamiento del resto.</p>
   </li>
</ul>