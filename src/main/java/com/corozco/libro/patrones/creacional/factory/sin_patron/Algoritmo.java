package com.corozco.libro.patrones.creacional.factory.sin_patron;

public enum Algoritmo {
    PS256, PS512, RS256, RS512
}
