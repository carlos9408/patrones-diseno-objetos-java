package com.corozco.libro.patrones.creacional.abstract_factory.dominio;

public interface UnidadNaval {

    void navegar();

    void atacar();
}
