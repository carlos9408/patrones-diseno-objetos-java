package com.corozco.libro.patrones.creacional.abstract_factory.dominio;

public interface UnidadTierra {

    void atacar();

    void defender();
}
