<h1 style="text-align:center;"> <strong>Singleton (Instancia Única)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Singleton</strong> limita la creación de un objeto a una sola instancia. Este patrón es considerado uno de los más sencillos, ya que su implementación es directa y no requiere de procedimientos complejos o técnicas avanzadas de desarrollo.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> En ocasiones es útil que una clase pueda ser instanciada exactamente una vez, por ejemplo: cuando se busca crear la conexión a una base de datos, definir variables de acceso global, o controlar el procesamiento de datos concurrentes a través de un recurso de acceso común.</p>

----

<h2><strong>Modelo UML</strong><br></h2>
<p style="text-align:justify;">De acuerdo con el modelo presentado en la siguiente figura, la clase <strong>Singleton</strong> asocia una instancia de sí mismo mediante una relación reflexiva. El constructor privado evita la creación de nuevas instancias y el método <strong>getInstancia()</strong> encapsula la lógica para que la clase solo se pueda instanciar una vez.</p>
<p style="text-align:center;">
<img src="../../../../../../../resources/images/creacional/singleton.png" width="70%" height="auto" alt="Modelo UML de Singleton">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Singleton:</strong> Clase que establece la lógica para controlar la creación de una sola instancia de sí misma.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Garantiza que solo existe una instancia de la clase y ofrece un punto de acceso global a ella, facilitando el control sobre cómo y cuándo se accede.</p>
   </li>

   <li>
      <p style="text-align:justify;">Al limitar la clase a una sola instancia, se reduce el uso innecesario de recursos, lo que es particularmente beneficioso para operaciones o recursos costosos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la gestión de recursos comunes o configuraciones a lo largo de la aplicación sin necesidad de pasar la instancia de la clase por todo el sistema.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite la inicialización perezosa de la instancia, ya que ésta es creada solo cuando se necesita, optimizando el proceso de arranque y la gestión de recursos en el sistema.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<p style="text-align:justify;">El patrón Singleton tiene varios detractores que han presentado argumentos para demostrar que el uso de este patrón es inadecuado y viola algunos de los principios más importantes del diseño orientado a objetos. El objetivo de este libro no es dar una opinión sobre si se debe utilizar o no, limitando su alcance a presentar la solución y dejar su uso al criterio del lector. A continuación, se presentan algunas de las desventajas o complicaciones más conocidas cuando se utiliza el patrón Singleton:</p>

<ul>
   <li>
      <p style="text-align:justify;">Al servir como punto de acceso global y controlar su propia instancia, el Singleton puede acabar cumpliendo múltiples roles.</p>
   </li>

   <li>
      <p style="text-align:justify;">El uso de instancias globales puede complicar las pruebas, especialmente cuando las pruebas requieren diferentes configuraciones o estados del objeto.</p>
   </li>

   <li>
      <p style="text-align:justify;">Sin una implementación cuidadosa, se pueden presentar problemas de concurrencia o instanciación múltiple en entornos multihilo.</p>
   </li>

   <li> 
      <p style="text-align:justify;">El patrón puede hacer que el código sea más difícil de refactorizar o extender, especialmente cuando la aplicación evoluciona y la instancia única ya no es adecuada.</p>
   </li>

</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Gestión de conexiones de base de datos:</strong> Limitar el número de conexiones abiertas y reutilizar una única conexión en toda la aplicación para optimizar recursos.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Configuración global de la aplicación:</strong> Proporcionar un punto de acceso central para gestionar y acceder a la configuración global utilizada en diferentes partes de la aplicación.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Registro (Logging):</strong> Centralizar la escritura de logs de la aplicación en un único sistema de registro para facilitar el seguimiento y la auditoría.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Manejo de caché:</strong> Almacenar objetos costosos de crear o recuperar en un único punto de acceso, mejorando el rendimiento y la eficiencia.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Controladores de hardware:</strong> Gestionar las interacciones con dispositivos de hardware para asegurar un estado consistente y operaciones seguras entre hilos.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Servicios de aplicación:</strong> Como servicios de autenticación, donde se requiere un punto único para la gestión de credenciales y control de acceso.</p>
   </li>
</ul>
