<h1 style="text-align:center;"> <strong>Builder (Constructor)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Builder</strong> facilita la creación de objetos complejos a través de un intermediario que permite asignar valores a sus atributos de forma dinámica.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> En general, un objeto puede tener múltiples combinaciones de sus atributos. El patrón <strong>Builder</strong> tiene como propósito crear objetos de manera dinámica evitando la declaración de constructores. Como resultado, el código se vuelve mas fácil de leer y mantener.</p>

----

<h2><strong>Modelo UML</strong><br></h2>
A continuación, se presentan algunas de las soluciones mas comunes para modelar el patrón <strong>Builder</strong>:
<ul>
   <li>
      <p style="text-align:justify;"> <strong>Builder explícito usando una interfaz:</strong> La interfaz <strong>IBuilder</strong> declara las operaciones para crear un objeto de forma dinámica. El <strong>BuilderConcreto</strong> define la lógica que permite instanciar un objeto de tipo <strong>Producto</strong> solicitado por el <strong>Cliente</strong>.</p>
      <br><br>
      <p style="text-align:center;">
        <img src="../../../../../../../resources/images/creacional/builder_abstracto.png" width="70%" height="auto" alt="">
      </p>
      <br>
</ul>
<ul>
   <li>
      <p style="text-align:justify;"> <strong>Builder explícito directo:</strong> Al no depender de una abstracción, el <strong>Builder</strong> explícito crea un grado de acoplamiento entre el cliente y el <strong>Producto</strong>. En este caso particular, el <strong>Cliente</strong> interactúa directamente con la clase <strong>BuilderConcreto</strong> que implementa la lógica necesaria para encapsular la instanciación de un <strong>Producto</strong>.</p>
      <br><br>
      <p style="text-align:center;">
         <img src="../../../../../../../resources/images/creacional/builder_concreto.png" width="70%" height="auto" alt="">
      </p>
      <br>
</ul>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Builder implicito:</strong> En algunas ocasiones, la solución mas adecuada consiste en crear un grado de acoplamiento directo entre el <strong>Producto</strong> y el <strong>BuilderConcreto</strong>. De este modo, el <strong>BuilderConcreto</strong> es un miembro estático de la clase <strong>Producto</strong>. La relación que une la clase <strong>Producto</strong> y el <strong>BuilderConcreto</strong> es una composición anidada, es decir, la clase <strong>BuilderConcreto</strong> es una clase estática que se encuentra en el mismo archivo que la clase <strong>Producto</strong>.</p>
      <br><br>
      <p style="text-align:center;">
         <img src="../../../../../../../resources/images/creacional/builder_implicito.png" width="70%" height="auto" alt="">
      </p>
      <br>
</ul>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Producto:</strong> Instancia solicitada por el cliente.</p>
   <li>
      <p style="text-align:justify;"><strong>IBuilder:</strong> Contrato que declara las operaciones que se deben realizar para asociar cada campo de la clase <strong>Producto</strong>.</p>
      <p>
      <p style="text-align:justify;"><strong>BuilderConcreto:</strong> Subclase que define las operaciones para asociar cada atributo, asegurando que siempre devuelve una instancia de sí mismo. El método <strong>build()</strong> retorna la instancia del objeto solicitado por el cliente.</p>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Facilita la construcción de objetos complejos paso a paso y permite cambiar la representación final del objeto sin cambiar el proceso de construcción.</p>
   </li>

   <li>
      <p style="text-align:justify;">Centraliza la creación y configuración de un objeto complejo en un lugar, lo que mejora la legibilidad y la gestión del código.</p>
   </li>

   <li>
      <p style="text-align:justify;">Ofrece la flexibilidad de construir un objeto en varias etapas y de diferentes maneras, lo que es especialmente útil cuando un objeto debe ser creado con muchas posibles configuraciones.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite a los constructores y los productos trabajar independientemente, mejorando así la modularidad del código.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Para objetos que no son inherentemente complejos, utilizar el patrón Builder puede añadir una complejidad innecesaria.</p>
   </li>

   <li>
      <p style="text-align:justify;">Si el número de tipos de objetos es grande, puede resultar en un gran número de clases Builder diferentes, lo que puede aumentar la complejidad del mantenimiento.</p>
   </li>

   <li>
      <p style="text-align:justify;">Si el Builder no completa todos los pasos necesarios antes de devolver el producto, puede resultar en objetos parcialmente inicializados.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Creación de objetos complejos:</strong> Como la construcción de documentos HTML o SQL complejos, donde cada parte del documento puede ser construida paso a paso y el mismo proceso puede generar diferentes representaciones.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Configuraciones de objetos con múltiples parámetros:</strong> Para objetos que requieren una configuración detallada de muchos campos, especialmente si muchos de estos campos son opcionales.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Combinación de productos:</strong> En escenarios donde los productos finales necesitan ser combinados con otros productos o configuraciones de manera flexible.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Construcción paso a paso:</strong> En aplicaciones de modelado de información de construcción (BIM) y software CAD, donde la representación de un objeto puede ser definida y modificada en distintas etapas del diseño.</p>
   </li>
</ul>