<h1 style="text-align:center;"> <strong>Prototype (Prototipo)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón Prototype permite clonar instancias de una clase utilizando un objeto como plantilla. Para lograrlo, el prototipo toma como base una estructura previa y realiza una copia de sus propiedades, las cuales son asignadas a un nuevo objeto del mismo tipo.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> En distintas ocasiones el costo de crear un nuevo objeto desde cero puede ser muy alto, en estas situaciones es adecuado clonar una estructura existente que actúe como un prototipo, y modificar sólo los atributos que sean necesarios. El patrón Prototype es útil cuando un cliente necesita nuevas instancias de un objeto de forma dinámica.</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;">La interfaz <strong>IPrototipo</strong> declara el cuerpo de la operación encargada de crear el nuevo objeto. La subclase <strong>PrototipoImpl</strong> implementa la lógica necesaria para copiar los campos que se van a utilizar en el nuevo objeto.</p>


<p style="text-align:center;">
<img src="../../../../../../../resources/images/creacional/prototype.png" width="70%" height="auto" alt="">
</p>

En general, existen dos tipos de clonación cuando se trabaja en el paradigma orientado a objetos, los cuales se
mencionan a continuación:

<ul>
   <li>
      <p style="text-align:justify;"><strong>Clonación superficial:</strong> El proceso de instanciación se limita a crear una copia de los atributos primitivos (short, int, long, float, double, char, boolean, byte), mientras que los tipos de datos compuestos (objetos) son una referencia al objeto original. Como resultado, cualquier cambio realizado en un atributo compuesto del objeto clonado afectará al objeto original.</p>
   <li>
      <p style="text-align:justify;"><strong>Clonación profunda:</strong> Se realiza una copia campo a campo del objeto original, resultando en una instancia diferente miembro a miembro. Esto implica un costo de procesamiento mayor debido a que el sistema debe crear la estructura completa desde cero. Sin embargo, modificar la copia no tiene ninguna consecuencia en el objeto original.</p>
</ul>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IPrototipo:</strong> Contrato que define la acción <strong>clonar()</strong>.</p>
   <li>
      <p style="text-align:justify;"><strong>PrototipoImpl:</strong> Subclase que debe implementar el comportamiento necesario para clonar el objeto.</p>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Permite la creación eficiente de objetos duplicando instancias existentes, lo cual puede ser más rápido que crear nuevas instancias desde cero, especialmente cuando la inicialización implica operaciones costosas.</p>
   </li>

   <li>
      <p style="text-align:justify;">Los objetos pueden ser clonados y modificados para adaptarse a necesidades específicas sin requerir un acoplamiento fuerte a la clase de los objetos a crear.</p>
   </li>

   <li>
      <p style="text-align:justify;">Al permitir configurar y clonar preconfiguraciones de objetos, reduce la necesidad de crear subclases específicas para configuraciones particulares.</p>
   </li>

   <li>
      <p style="text-align:justify;">Al centralizar la lógica de clonación, se reduce la repetición de código y se simplifica el manejo de la creación de objetos complejos.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Clonar objetos que contienen referencias a otros objetos (clonación profunda) puede ser complicado y propenso a errores si no se maneja correctamente.</p>
   </li>

   <li>
      <p style="text-align:justify;">La clonación puede romper el encapsulamiento si los objetos clonados modifican objetos internos compartidos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Requiere la implementación de una interfaz de clonación que cada clase debe definir adecuadamente, lo cual puede ser tedioso para objetos complejos.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Preconfiguraciones de objetos:</strong> En sistemas donde se necesita crear instancias de objetos con configuraciones específicas de manera repetitiva, permitiendo definir prototipos y clonarlos según sea necesario.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Operaciones de deshacer:</strong> Para implementar funcionalidades de deshacer/rehacer en aplicaciones, donde se pueden clonar estados de objetos para restaurarlos posteriormente.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Instanciación costosa de objetos:</strong> En situaciones donde la creación de nuevas instancias es costosa en términos de recursos o tiempo, como objetos que requieren carga de datos o configuraciones complejas.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Sistemas de juegos:</strong> Para la creación rápida de copias de entidades de juego, como personajes o ítems, que pueden ser personalizados o modificados independientemente después de ser clonados.</p>
   </li>
</ul>