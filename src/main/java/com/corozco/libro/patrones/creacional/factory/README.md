<h1 style="text-align:center;"> <strong>Factory (Fábrica)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Factory</strong> permite crear objetos que pertenecen a una jerarquía de clases sin conocer su tipo a priori.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> En general, los lenguajes que siguen el paradigma orientado a objetos permiten la creación de instancias concretas de una superclase a través de la clausula <strong>new()</strong>. Sin embargo, en muchas ocasiones el cliente es quien determina cual es la subclase que necesita en tiempo de ejecución.</p>

----

<h2><strong>Modelo UML</strong><br></h2>
El cliente solicita un <strong>ProductoConcreto</strong> a la interfaz <strong>IProductoCreador</strong>. La interfaz
delega la creación del <strong>ProductoConcreto</strong> a cada <strong>ProductoCreador</strong>.


<p style="text-align:center;">
    <img src="../../../../../../../resources/images/creacional/factory.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Producto:</strong> Instancia solicitada por el cliente.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ProductoConcreto:</strong> Subclase de la clase <strong>Producto</strong>.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>IProductoCreador:</strong> Contrato que declara la operación necesaria para crear un <strong>Producto</strong>.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ProductoCreador:</strong> Subclase que retorna el <strong>Producto</strong> solicitado por el cliente.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Al definir una interfaz común para crear objetos, se pueden agregar nuevas clases de productos sin alterar el código que utiliza la fábrica.</p>
   </li>

   <li>
      <p style="text-align:justify;">Los clientes operan con interfaces o clases abstractas en lugar de clases concretas, reduciendo la dependencia directa entre el código que solicita un objeto y el código que lo crea.</p>
   </li>

   <li>
      <p style="text-align:justify;">Se puede cambiar o extender las clases que crea la fábrica sin cambiar el código que las usa.</p>
   </li>

   <li>
      <p style="text-align:justify;">Asegura que todos los objetos de un tipo se creen siguiendo un proceso uniforme, facilitando su manejo y configuración a medida que el sistema se extiende.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Introducir una capa adicional de abstracción con fábricas puede complicar el diseño si no se justifica por una suficiente variabilidad en la creación de objetos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Crear un sistema de fábricas y productos que sea demasiado general puede hacer que el diseño sea difícil de entender y mantener cuando las solución comienza a crecer.</p>
   </li>

   <li>
      <p style="text-align:justify;">Aunque el patrón busca reducir el acoplamiento, en ciertos casos, los clientes aún necesitan conocer las subclases para configurar la fábrica adecuadamente.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Creación de componentes de UI:</strong> En aplicaciones que deben crear elementos de la interfaz de usuario que varían según el sistema operativo o las preferencias del usuario, permitiendo que la apariencia de la aplicación se adapte dinámicamente.
   </li>

   <li>
      <p style="text-align:justify;"><strong>Manejo de recursos en juegos:</strong> Para juegos que necesitan instanciar diferentes tipos de personajes o entornos según el nivel o el mundo, facilitando la extensión y la personalización.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Aplicaciones con plugins o extensiones:</strong> En sistemas que deben ser extensibles y permitir la incorporación de nuevas funcionalidades o tipos de datos mediante plugins, el patrón <strong>Factory</strong> puede ser usado para crear instancias de estos tipos dinámicamente.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Herramientas de software para manipulación de documentos:</strong> Soluciones que necesitan crear diferentes tipos de documentos o procesar varios formatos de archivo.</p>
   </li>
</ul>