<h1 style="text-align:center;"> <strong>Patrones Creacionales</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> 
    Los patrones creacionales se centran en abstraer el proceso de creación de nuevos objetos o estructuras complejas. Su objetivo es permitir que el sistema sea independiente de cómo los objetos son creados y representados dentro del dominio de la aplicación. Para alcanzar este fin, los patrones creacionales ofrecen un conjunto de técnicas que aíslan la lógica de creación de objetos. Esto permite que dicha lógica pueda ser reutilizada de manera transparente por cualquier cliente, eliminando la necesidad de preocuparse por los detalles de su implementación interna.
</p>

<p style="text-align:justify;">
Los patrones creacionales se rigen bajo tres ideas principales: (i) encapsular la estructura interna de un objeto, (ii) ocultar los mecanismos utilizados para su instanciación y (iii) tratar ---en medida de lo posible--- de ser genéricos. Gracias a ello, un desarrollador solo se debe preocupar por solucionar problemas y/o requerimientos directamente relacionados con el dominio de la aplicación, delegando la responsabilidad de la creación de nuevas estructuras complejas a un patrón, garantizando que las soluciones sean altamente flexibles.
</p>

----

<h2><strong>Importancia</strong><br></h2>
<p style="text-align:justify;"> 
    Durante el desarrollo de nuevas funcionalidades, es común encontrarse con escenarios en los que cada desarrollador propone soluciones basadas en su experiencia, dominio del lenguaje de programación, capacidad de análisis, entre otros factores. Esto es natural, dado que cada individuo es único y cada problema puede ser abordado desde distintas perspectivas. No obstante, se ha notado que muchos de los problemas que surgen a lo largo del ciclo de vida de los proyectos tienden a seguir patrones o comportamientos comunes y recurrentes. En este contexto, los patrones creacionales fueron diseñados con el propósito de ofrecer estrategias que permitan estandarizar los procesos de creación de nuevos objetos de manera dinámica.
</p>

<p style="text-align:justify;"> 
    De acuerdo con lo anterior, los patrones creacionales ofrecen una base sólida para el desarrollo de software eficiente y mantenible. Su importancia radica en la capacidad de simplificar el diseño de software al reducir el acoplamiento entre los componentes del sistema. Esto se logra mediante la abstracción del proceso de creación de objetos, permitiendo que los cambios en la manera de crear objetos o en su representación interna tengan un impacto mínimo en el código cliente. Como resultado, los desarrolladores pueden enfocarse en la lógica de negocio principal, mejorando la calidad del código y facilitando tanto la prueba como la extensión del sistema. Además, la implementación de estos patrones mejora la reusabilidad del código y fomenta una estructura de software más ordenada y escalable, lo que es crucial para el desarrollo de aplicaciones complejas y de larga duración.
</p>

----

<h2><strong>Listado de patrones</strong><br></h2>

<h3>- [Singleton (Instancia Única)](singleton/README.md)</h3>
<h3>- [Prototype (Prototipo)](prototype/README.md)</h3>
<h3>- [Builder (Constructor)](builder/README.md)</h3>
<h3>- [Factory (Fábrica)](README.md)</h3>
<h3>- [Abstract Factory (Fábrica Abstracta)](abstract_factory/README.md)</h3>


----