package com.corozco.libro.patrones.comportamiento.iterator.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Producto {

    private long id;
    private String codigo;
}
