package com.corozco.libro.patrones.comportamiento.iterator;

public interface Iterable {

    Iterador iterador();
}
