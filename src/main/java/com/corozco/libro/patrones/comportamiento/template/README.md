<h1 style="text-align:center;"> 
    <strong>Template Method (Método Plantilla)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Template Method</strong> provee una plantilla por defecto ---superclase--- para la implementación de un conjunto de pasos claramente definidos que pueden ser sobrescritos por sus subclases.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
En muchos casos, la solución a un problema sigue siempre el mismo flujo de operaciones. El patrón <strong>Template Method</strong> propone una solución elegante a través de la definición de una plantilla que representa la solución general a un problema, pero que puede ser personalizada por cada una de sus subclases mediante operaciones de sobreescritura. 
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La clase <strong>Plantilla</strong> establece el flujo genérico para solucionar un problema. Cada <strong>Implementación</strong> sobrescribe las operaciones que considere convenientes para garantizar su funcionamiento.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/template_method.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Plantilla:</strong>  Clase que define el comportamiento general de la solución a un problema.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Implementación:</strong> Subclase que sobrescribe las operaciones que varían con respecto a la plantilla.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Centraliza el código común, reduciendo la duplicación y fomentando la reutilización del código, lo que facilita su mantenimiento y actualización.</p>
   </li>

   <li>
      <p style="text-align:justify;">Ofrece una guía clara para el diseño de futuras implementaciones al definir un esqueleto de algoritmo, lo que mejora la coherencia y la calidad del diseño del software.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite la variabilidad de ciertos pasos del algoritmo en tiempo de ejecución, ofreciendo flexibilidad para personalizar el comportamiento de las subclases.</p>
   </li>

   <li>
      <p style="text-align:justify;">Fomenta el principio de inversión de control, al dejar que las subclases decidan cómo implementar los pasos del algoritmo, mejorando la modularidad y la extensibilidad.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede resultar en una jerarquía de clases más complicada y difícil de entender, especialmente si se abusa del patrón y se crea una gran cantidad de subclases.</p>
   </li>   

   <li>
      <p style="text-align:justify;">Limita la flexibilidad para cambiar el esqueleto del algoritmo una vez que se ha definido, ya que hacer modificaciones en la estructura de la plantilla obliga a replicar los cambios en todas las subclases.</p>
   </li>

   <li>
      <p style="text-align:justify;">La implementación de métodos concretos en subclases puede llevar a una violación del principio de sustitución de Liskov si no se manejan adecuadamente las expectativas de la plantilla.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Frameworks de aplicaciones web:</strong> Para definir un flujo estándar de procesamiento de solicitudes HTTP, permitiendo a los desarrolladores personalizar el comportamiento de ciertas fases del procesamiento.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Procesamiento de documentos:</strong> En sistemas de generación de informes o procesamiento de documentos, donde el esqueleto del proceso puede ser común, pero los detalles de procesamiento específicos del documento necesitan ser definidos por subclases.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Juegos con múltiples fases:</strong> Para definir la estructura general de un juego que incluye fases como inicialización, juego en curso y finalización, permitiendo diferentes comportamientos en cada fase según el tipo de juego.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Algoritmos en librerías de software:</strong> Como en librerías de ordenamiento o algoritmos de búsqueda, donde la estructura general del algoritmo es fija, pero ciertos pasos, como la comparación de elementos, pueden ser personalizados.</p>
   </li>
</ul>