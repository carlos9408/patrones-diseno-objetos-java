<h1 style="text-align:center;"> 
    <strong>State (Estado)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>State</strong> modifica el comportamiento de un objeto de acuerdo con el estado en el que se encuentre en un momento determinado.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
En general, los objetos en un sistema pueden realizar diferentes acciones cuando se cumplen ciertas condiciones. Una condición que cambie la estructura interna de un objeto se considera un estado.
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La clase <strong>Contexto</strong> instancia y modifica el comportamiento de la interfaz <strong>IEstado</strong>. La subclase <strong>EstadoConcreto</strong> realiza las acciones asociadas con un estado específico que toma el objeto en tiempo de ejecución.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/state.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IEstado:</strong> Interfaz representa los estados posibles de un objeto.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>EstadoConcreto:</strong> Subclase que implementa las operaciones relacionadas con un estado particular.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>Contexto:</strong> Clase que gestiona un objeto de acuerdo con su estado.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>ObservableConcreto:</strong> Subclase que implementa el comportamiento definido por un observable cuando debe notificar una actualización a los observadores que tiene suscritos.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Encapsula las variaciones de comportamiento para diferentes estados dentro de clases separadas, mejorando la cohesión y haciendo el cambio de estado explícito.</p>
   </li>

   <li>
      <p style="text-align:justify;">Simplifica el código del objeto al eliminar la necesidad de grandes estructuras condicionales para cambiar comportamientos en función del estado.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la adición de nuevos estados y comportamientos, promoviendo la extensibilidad del código sin modificar los estados existentes o la lógica del contexto.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite cambiar el comportamiento de un objeto en tiempo de ejecución según su estado, ofreciendo una alternativa flexible a la herencia.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede aumentar el número de clases, ya que cada estado se implementa como una clase separada, lo que podría complicar el mantenimiento si el número de estados es muy alto.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La lógica de transición entre estados puede volverse compleja si hay muchas reglas para cambiar de un estado a otro, especialmente si estas transiciones dependen de condiciones complicadas o acciones externas.</p>
   </li>

   <li>
      <p style="text-align:justify;">El uso inapropiado del patrón (por ejemplo, abusar de él para situaciones que no cambian de estado frecuentemente) puede resultar en una sobrecarga innecesaria y complicaciones en el diseño.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Juegos:</strong> Para manejar el estado de los personajes o del juego mismo, donde el comportamiento puede cambiar radicalmente dependiendo del estado actual, como en modos de invulnerabilidad, diferentes niveles de energía o estados de movimiento.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Interfaces de usuario (UI):</strong> Para controlar el estado de los elementos de la interfaz, como botones o menús que cambian su comportamiento y apariencia según el estado de la aplicación o la interacción del usuario.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Workflow de aplicaciones:</strong> En sistemas de gestión de procesos de negocio donde las entidades pasan por varios estados, cada uno con reglas de procesamiento únicas, como en sistemas de aprobación de documentos o gestión de pedidos.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Control de conexiones de red:</strong> Para manejar los diferentes estados de una conexión de red (conectado, desconectado, reconectando), permitiendo a la aplicación adaptar su comportamiento en respuesta a cambios en el estado de la conexión.</p>
   </li>
</ul>