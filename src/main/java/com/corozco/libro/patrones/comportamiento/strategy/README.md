<h1 style="text-align:center;"> 
    <strong>Strategy (Estrategia)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Strategy</strong> ofrece a una clase la posibilidad de modificar su comportamiento en tiempo de ejecución, esto se logra mediante la definición de una familia de algoritmos que pueden ser utilizados de manera transparente para un usuario.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
La mayoría de los sistemas ofrecen a un cliente la posibilidad de adaptar su experiencia en tiempo de ejecución. En particular, el patrón \textbf{Strategy} surge debido a la necesidad de facilitar la inyección de dependencias, permitiendo una mayor flexibilidad y extensibilidad. Este patrón destaca por su capacidad para cambiar algoritmos o comportamientos en tiempo de ejecución sin alterar el código que los utiliza.
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La clase <strong>Contexto</strong> se encarga de instanciar el algoritmo (estrategia) solicitado por un cliente. La interfaz <strong>IEstrategia</strong> declara el cuerpo para las operaciones que debe realizar cada <strong>EstrategiaConcreta</strong>. </p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/strategy.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IEstrategia:</strong> Interfaz que declara el cuerpo de las operaciones realizadas por cada estrategia concreta.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>EstrategiaConcreta:</strong> Subclase que representa el detalle de una solución particular.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>Contexto:</strong> Clase que instancia la estrategia solicitada por un cliente en tiempo de ejecución.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Ofrece una alta flexibilidad al permitir cambiar los algoritmos dinámicamente en tiempo de ejecución según las necesidades del momento.</p>
   </li>

   <li>
      <p style="text-align:justify;">Reduce el acoplamiento entre el contexto y las estrategias específicas, siguiendo el principio de diseño de programar contra interfaces, no implementaciones.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la extensión del software al permitir la incorporación de nuevas estrategias sin modificar el contexto en el que se utilizan.</p>
   </li>

   <li>
      <p style="text-align:justify;">Mejora la legibilidad y organización del código al separar la implementación de diferentes algoritmos de la lógica del contexto que los utiliza.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">La aplicación del patrón puede llevar a la creación de múltiples clases pequeñas (una por cada estrategia), lo que podría complicar la estructura del proyecto.</p>
   </li>   

   <li>
      <p style="text-align:justify;">Los clientes del patrón deben ser conscientes de las diferencias entre las estrategias para elegir la adecuada, lo cual puede aumentar la complejidad del uso.</p>
   </li>

   <li>
      <p style="text-align:justify;">Si las estrategias son similares, se puede duplicar código de forma innecesaria.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Selección de algoritmos de encriptación:</strong> En aplicaciones que necesitan encriptar datos, donde diferentes situaciones requieren el uso de diferentes algoritmos de encriptación.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Estrategias de compresión de datos:</strong> Para sistemas que almacenan o transmiten datos y pueden beneficiarse de diferentes algoritmos de compresión basados en el tipo de datos o en restricciones de ancho de banda.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Cálculo de rutas en aplicaciones de navegación:</strong> Donde diferentes criterios (el camino más corto, el más rápido, el menos costoso) pueden ser seleccionados por el usuario.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Algoritmos de renderizado en sistemas gráficos:</strong> En aplicaciones de diseño gráfico o juegos, donde diferentes técnicas de renderizado pueden ser elegidas dependiendo de las capacidades del hardware o preferencias del usuario.</p>
   </li>
</ul>