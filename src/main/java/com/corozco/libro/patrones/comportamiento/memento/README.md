<h1 style="text-align:center;"> 
    <strong>Memento (Memoria)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Memento</strong> encapsula el estado de un objeto en diferentes instantes del tiempo. Gracias a esto, el patrón dota a un objeto con la capacidad de volver a un estado anterior.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
En ocasiones, es necesario que el sistema tenga la capacidad de volver a estados previos por motivos de trazabilidad y seguimiento. El patrón <strong>Memento</strong> es básicamente la solución a un problema que requiere la implementación de la operación "ctrl+z".
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> la clase <strong>Cuidador</strong> almacena instancias de tipo <strong>Memento</strong> y provee las operaciones necesarias para acceder y/o retornar una instancia solicitada por un cliente. El <strong>Originador</strong> almacena el estado actual del objeto y permite restaurarlo a un estado anterior.</p>

----

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/memento.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Originador:</strong> Clase que puede retornar a un estado previo. </p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Memento:</strong> Clase que almacena el estado de un <strong>Originador</strong> en un instante de tiempo.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>Cuidador:</strong> Clase que contiene la lista de mementos con la información de estados previos del <strong>Originador</strong>.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Permite guardar estados de un objeto para su posterior restauración, facilitando la implementación de operaciones de deshacer (undo) o rehacer (redo) sin violar el encapsulamiento.</p>
   </li>

   <li>
      <p style="text-align:justify;">Ofrece una forma clara y sencilla de capturar el estado de un objeto en un momento dado, lo que puede ser útil para la serialización o el mantenimiento de versiones.</p>
   </li>

   <li>
      <p style="text-align:justify;">Ayuda a mantener la integridad del objeto principal al permitir que el objeto memento maneje el almacenamiento del estado, separando el almacenamiento del estado de la lógica del objeto.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede aumentar el uso de memoria si los objetos que se deben restaurar son grandes o complejos. De igual manera, almacenar muchos objetos puede reducir el rendimiento general de la aplicación.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La gestión de los objetos memento puede complicarse, especialmente si hay una necesidad de limitar el número de estados guardados o de implementar mecanismos de limpieza.</p>
   </li>

   <li>
      <p style="text-align:justify;">Restaurar estados anteriores puede resultar en dependencias ocultas si el objeto ha interactuado con otros objetos que no han revertido su estado, lo que podría llevar a inconsistencias.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Editores de texto y gráficos:</strong> En aplicaciones de chat, donde múltiples usuarios interactúan entre sí, el mediador gestiona el envío de mensajes a los participantes adecuados.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de control y gestión de interfaces de usuario:</strong> Para manejar eventos complejos generados por componentes de la interfaz de usuario, donde el mediador coordina las acciones a realizar en respuesta a esos eventos.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Aplicaciones de juego:</strong> En juegos con múltiples actores o entidades, el mediador puede gestionar las interacciones entre ellos, como el combate, el comercio o las alianzas.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Manejo de transacciones:</strong> En sistemas que requieren garantizar la atomicidad de las operaciones, utilizando mementos para restaurar el estado de los objetos en caso de fallos o errores.</p>
   </li>
</ul>