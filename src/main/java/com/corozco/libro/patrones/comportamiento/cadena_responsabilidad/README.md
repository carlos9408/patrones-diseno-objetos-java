<h1 style="text-align:center;"> 
    <strong>Chain of Responsability</strong> <br>
    <strong>(Cadena de Responsabilidad)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Chain of Responsability</strong> permite que una petición pase por diferentes etapas antes de llegar a su destino. Cada etapa está a cargo de un manejador (handler) habilitado para determinar si detiene la petición o la pasa al siguiente manejador en la cadena. De esta forma, una petición puede ser resuelta antes de llegar al destino.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Chain of Responsability</strong> sigue el principio de \quotes{evitar acoplar una solicitud con el destinatario de forma directa}. Separar una petición en etapas permite establecer una hoja de ruta que puede recorrer un objeto antes de llegar a su destino. Lo anterior es útil cuando un problema se puede dividir en fases, p. ej.: el proceso de validación de una transacción bancaria o el tramite de un documento que debe ser aprobado por varios entes de control antes que la dependencia encargada de emitirlo lo pueda autorizar.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La clase <strong>Manejador</strong> declara el contrato para procesar una petición. Cada <strong>ManejadorConcreto</strong> tiene dos opciones: (i) resuelve la petición (éxito o error) o (ii) pasa la petición al siguiente manejador que tenga designado.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/chain_of_responsability.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Manejador:</strong> Clase que declara las operaciones necesarias para procesar una petición.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ManejadorConcreto:</strong> Subclase que representa una etapa específica durante el flujo de la petición. El manejador decide si es capaz de resolver la solicitud o debe pasarla al siguiente manejador en la línea de responsabilidad.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Reduce el acoplamiento al permitir que múltiples objetos tengan la oportunidad de manejar una solicitud, sin que el emisor de la solicitud conozca el receptor.</p>
   </li>

   <li>
      <p style="text-align:justify;">Aumenta la flexibilidad en la asignación de responsabilidades a objetos. Los manejadores pueden ser agregados o reorganizados dinámicamente sin cambiar el código del emisor.</p>
   </li>

   <li>
      <p style="text-align:justify;">Simplifica el código al separar los manejadores en clases independientes, cada una con una única tarea o responsabilidad.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite un conjunto variado de objetos capaces de manejar una solicitud, aumentando la extensibilidad del manejo de solicitudes.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">No garantiza la captura de la solicitud: una solicitud puede terminar sin ser manejada si no hay ningún manejador apropiado en la cadena.</p>
   </li>   

   <li>
      <p style="text-align:justify;">Puede complicar el seguimiento del flujo de una solicitud a través de la cadena, especialmente en cadenas largas o complejas.</p>
   </li>

   <li>
      <p style="text-align:justify;">El rendimiento puede ser afectado si la cadena es larga o si el procesamiento de la solicitud es intensivo, ya que la solicitud debe pasar por varios manejadores antes de ser procesada.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Procesamiento de eventos en interfaces gráficas:</strong> Para manejar eventos generados por la interfaz de usuario, como clics de ratón o pulsaciones de teclas, donde diferentes objetos pueden ser responsables de diferentes tipos de eventos.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de autorización:</strong> En aplicaciones que requieren múltiples comprobaciones de autorización o permisos que pueden ser manejados por diferentes objetos según el nivel de acceso requerido.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Manejo de solicitudes HTTP:</strong> Para procesar solicitudes HTTP en servidores web, donde diferentes manejadores pueden procesar diferentes tipos de solicitudes, como autenticación, registro de solicitudes, y entrega de contenido.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Flujos de trabajo de validación:</strong> En sistemas que necesitan validar entradas o procesos a través de una serie de pasos o comprobaciones, donde cada paso puede ser manejado por un objeto diferente en la cadena.</p>
   </li>
</ul>