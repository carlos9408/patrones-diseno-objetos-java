<h1 style="text-align:center;"> 
    <strong>Mediator (Mediador)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Mediator</strong> permite desacoplar la comunicación entre objetos mediante la definición de un objeto que actúa como intermediario y se encarga de gestionar su interacción.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
El patrón <strong>Mediator</strong> viene bien cuando es necesario desacoplar objetos fuertemente relacionados. A medida que crece la cantidad de estructuras complejas en un sistema, el grado de dependencia entre sus componentes tiende a aumentar. El patrón <strong>Mediator</strong> ofrece un punto de encuentro que centraliza la comunicación entre diferentes objetos, evitando que interactúen directamente.
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La interfaz <strong>IMediador</strong> declara el cuerpo de las operaciones necesarias para que diferentes objetos se puedan comunicar entre sí. La subclase <strong>MediadorConcreto</strong> implementa el contrato definido por la interfaz. La clase <strong>Colega</strong> declara las operaciones que cada <strong>ColegaConcreto</strong> puede realizar a través del mediador.</p>


<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/mediator.png" width="70%" height="auto" alt="">
</p>


----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IMediador:</strong> Interfaz que centraliza la comunicación entre diferentes colegas.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>MediadorConcreto:</strong> Subclase que implementa las operaciones necesarias para redireccionar mensajes entre colegas.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>Colega:</strong> Clase o interfaz que define el cuerpo para la comunicación entre diferentes objetos relacionados.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>ColegaConcreto:</strong> Subclase que implementa la lógica necesaria para enviar o recibir un mensaje a través del mediador.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Reduce el acoplamiento entre clases al centralizar la comunicación compleja y las interdependencias en un objeto mediador, mejorando la mantenibilidad y la flexibilidad del código.</p>
   </li>

   <li>
      <p style="text-align:justify;">Simplifica la gestión de las interacciones entre objetos al encapsular la lógica de comunicación en una clase separada, facilitando la comprensión y la modificación del flujo de interacciones.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la reutilización de objetos al hacer que su interacción dependa de la abstracción del mediador en lugar de otras clases, lo que permite reutilizarlos en diferentes contextos sin cambios.</p>
   </li>

   <li>
      <p style="text-align:justify;">Mejora la capacidad de cambio y extensión del sistema al permitir modificar las interacciones entre objetos o introducir nuevos objetos ajustando únicamente el mediador, sin necesidad de revisar los objetos que participan en la comunicación.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">El objeto mediador puede volverse demasiado complejo al asumir demasiadas responsabilidades, lo que lo convierte en un punto único de fallo y complica su mantenimiento y comprensión.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La lógica de interacción concentrada en el mediador puede llevar a un rendimiento reducido, especialmente si el mediador se convierte en un cuello de botella en la comunicación entre objetos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Puede ser difícil depurar el sistema ya que el flujo de ejecución pasa a través del mediador, lo que puede hacer que el seguimiento de las interacciones sea más complicado.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Sistemas de chat o mensajería:</strong> En aplicaciones de chat, donde múltiples usuarios interactúan entre sí, el mediador gestiona el envío de mensajes a los participantes adecuados.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de control y gestión de interfaces de usuario:</strong> Para manejar eventos complejos generados por componentes de la interfaz de usuario, donde el mediador coordina las acciones a realizar en respuesta a esos eventos.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Aplicaciones de juego:</strong> En juegos con múltiples actores o entidades, el mediador puede gestionar las interacciones entre ellos, como el combate, el comercio o las alianzas.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Orquestación de microservicios:</strong> En arquitecturas basadas en microservicios, un mediador puede facilitar la comunicación y cooperación entre servicios, gestionando las dependencias y el flujo de datos entre ellos.</p>
   </li>
</ul>