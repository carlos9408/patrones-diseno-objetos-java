<h1 style="text-align:center;"> 
    <strong>Visitor (Visitante)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Visitor</strong> permite agregar operaciones a una jerarquía de clases que cumple lo siguiente: las operaciones definidas en la superclase pueden cambiar con el tiempo y el comportamiento de las subclases no lo hace.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
Para entender la importancia del patrón <strong>Visitor</strong> es necesario recordar los siguientes conceptos:  
</p>

<ul>
    <li>
       <p style="text-align:justify;"><strong>Principio abierto/cerrado:</strong> Una clase  facilita la creación de nuevas operaciones y evita al máximo modificar el código existente.</p>
   </li>
    <li>
       <p style="text-align:justify;"><strong>Principio de inversión de dependencias:</strong> El sistema debe depender de abstracciones en lugar de implementaciones.</p>
   </li>
</ul>

<p style="text-align:justify;"> El paradigma orientado a objetos propone de forma implícita que las abstracciones (en principio) son estables; es decir, no debería existir una buena razón para que una abstracción cambie. Por otro lado, cada implementación puede extender el alcance de la solución mediante operaciones de sobrecarga, sobreescritura de métodos, o a través de la creación de operaciones adicionales. En la siguiente figura, se presenta un ejemplo del modelo descrito anteriormente.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/visitor_problema.png" width="70%" height="auto" alt="">
</p>

<p style="text-align:justify;"> Sin embargo, en algunas ocasiones, la abstracción debe cambiar de manera frecuente por decisiones de negocio o simplemente porque la solución lo requiere, y las implementaciones concretas son estables con el paso del tiempo. Este tipo de casos (aunque suceden rara vez) traen como consecuencia la modificación sistemática de cada una de las subclases que extienden la abstracción. Para evitar esto, el patrón <strong>Visitor</strong> propone crear una estructura adicional que permite agregar nuevas operaciones al modelo sin impactar a la abstracción.
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La interfaz <strong>IVisitante</strong> provee la lógica que permite a otras clases definir e invocar nuevas operaciones. La interfaz <strong>IAbstracción</strong> declara el método "aceptarVisitante()" que decide cual es la operación que deben realizar las implementaciones. Cada <strong>Implementación</strong> puede visitar a un <strong>VisitanteConcreto</strong> para realizar una operación determinada enviándose a sí mismo como parámetro.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/visitor.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IAbstraccion:</strong> Interfaz que habilita a todas sus subclases para aceptar visitantes.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Implementacion:</strong> Subclase que decide a quién debe visitar para ejecutar una acción.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>IVisitante:</strong> Interfaz que declara cada una de las operaciones nuevas en la abstracción.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>VisitanteConcreto:</strong> Subclase que ejecuta las operaciones solicitadas por una implementación.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Facilita la adición de nuevas operaciones sobre los elementos de una estructura sin modificarla, lo que mejora la extensibilidad.</p>
   </li>

   <li>
      <p style="text-align:justify;">Agrupa operaciones relacionadas y las separa de los objetos sobre los que operan, lo que ayuda a mantener el código organizado y promueve un alto nivel de cohesión.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite aplicar operaciones sobre estructuras de objetos complejas, como árboles, de manera conveniente y eficiente, utilizando la doble indirección.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la ejecución de diversas operaciones en múltiples tipos de elementos sin conocer cual es su clase específica.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede llevar a una violación del principio de encapsulamiento, ya que el patrón requiere que el visitante tenga acceso a los elementos internos de los objetos que visita.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La adición de nuevos tipos de elementos a la estructura puede requerir cambios en la interfaz del visitante y en todas sus implementaciones existentes, lo que reduce la flexibilidad.</p>
   </li>

   <li>
      <p style="text-align:justify;">Puede resultar en un diseño complejo y un código más difícil de entender, especialmente para aquellos no familiarizados con el patrón.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Manipulación y análisis de estructuras de árboles:</strong> Para realizar operaciones como renderización, optimización o transformación de estructuras de datos complejas, como los árboles de sintaxis en compiladores.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de reporte y exportación de datos:</strong> Donde diferentes formatos de reporte pueden ser generados a partir de un conjunto de objetos mediante visitantes específicos para cada formato.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Interfaces gráficas de usuario (GUI):</strong> En la implementación de operaciones que afectan a múltiples tipos de elementos de la GUI, como habilitar/deshabilitar o aplicar temas visuales de forma masiva.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Aplicaciones de análisis y mantenimiento de código:</strong> Para extraer métricas, realizar análisis estático o aplicar refactorizaciones sobre el código fuente representado como una estructura de objetos.</p>
   </li>
</ul>