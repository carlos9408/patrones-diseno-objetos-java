<h1 style="text-align:center;"> 
    <strong>Observer (Observador)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Observer</strong> permite establecer una relación de tipo "observable-observador", en la cual un objeto (observable) tiene la capacidad de informar eventos o actualizaciones a todos los objetos que se encuentren suscritos para recibir notificaciones (observadores).</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
Sin un observador, cada instancia que necesite conocer el estado de otro objeto debe realizar solicitudes periódicas, lo cual consume recursos, tiempo y puede generar cuellos de botella.
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La clase <strong>Observable</strong> declara las operaciones necesarias para almacenar y notificar a los observadores. El <strong>ObservableConcreto</strong> se encarga de notificar a todos los observadores que tiene asociados. La interfaz <strong>IObservador</strong> define el contrato para que un <strong>ObservadorConcreto</strong> pueda ser notificado ante cualquier cambio al que esté suscrito.</p>

<p style="text-align:justify;"> La relación entre la interfaz <strong>IObservable</strong> y la clase <strong>Observador</strong> puede tener varias interpretaciones a nivel conceptual, las cuales se mencionan a continuación:</p>

<ul>
    <li>
        <p style="text-align:justify;">Un observable y un observador pueden existir de forma independiente y nunca estar relacionados, p. ej.: un comprador (observador) frecuente de una librería (observable) puede pasar toda su vida sin recibir actualizaciones cuando llega un nuevo libro. Lo anterior constituye una relación de asociación.</p>
    </li>
    <li>
        <p style="text-align:justify;">Un observable y un observador pueden existir de forma independiente pero debe existir una relación entre ellos en algún momento, p. ej.: un detector de incendios conectado a un sensor en linea (observable) debe notificar a los miembros de una casa (observadores) cuando existe riesgo de fuego. Los habitantes pueden elegir nunca tener un detector de incendios, pero el conducto regular los induce a que deben tenerlo para evitar dolores de cabeza. Lo anterior constituye una relación de agregación.</p>
    </li>
    <li>
        <p style="text-align:justify;">Un observable y un observador no pueden existir de forma independiente, p. ej.: un curso (observable) no se puede abrir y notificar notas sin al menos un estudiante inscrito (observador) Lo anterior constituye una relación de composición.</p>
    </li>
</ul>

<p style="text-align:justify;"> La relación <strong>IObservable</strong>/<strong>Observador</strong> descrita en el modelo presentado en la siguiente figura se diseñó a través de agregación solo para ilustrar que la relación entre los dos conceptos es flexible. Sin embargo, su implementación en un contexto real depende del problema.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/observer.png" width="70%" height="auto" alt="">
</p>

<p style="text-align:justify;">
    La idea anterior surge como un símil al impulso de los científicos de asumir que las vacas son esféricas para simplificar sus cálculos. Sin embargo, como ingenieros de software, nuestra realidad dista de los ideales. En este sentido es crucial entender que, aunque los patrones de diseño buscan ofrecer soluciones genéricas, nos movemos en un mundo donde predominan las implementaciones concretas.
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IObservador:</strong> Contrato que representa un elemento del sistema habilitado para recibir notificaciones o actualizaciones.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ObservadorConcreto:</strong> Subclase que determina las acciones que se deben realizar cuando el observador recibe una nueva notificación.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>IObservable:</strong> Contrato que declara las operaciones necesarias para notificar a un <strong>ObservadorConcreto</strong> cuando existe un cambio que debe ser informado.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>ObservableConcreto:</strong> Subclase que implementa el comportamiento definido por un observable cuando debe notificar una actualización a los observadores que tiene suscritos.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Facilita la comunicación entre objetos al establecer una relación de suscripción entre el sujeto y los observadores, permitiendo que los cambios en el estado de un objeto se propaguen automáticamente a sus dependientes.</p>
   </li>

   <li>
      <p style="text-align:justify;">Promueve el principio de bajo acoplamiento al mantener a los objetos observadores separados y desacoplados del sujeto, lo que facilita la reutilización y extensión de los componentes.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la actualización dinámica de cambios, esto es útil cuando el estado de un objeto cambia y es necesario reflejar esos cambios en los objetos que tiene vinculados.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite variaciones en la cantidad y tipo de observadores sin modificar el sujeto, proporcionando flexibilidad en la gestión de las relaciones entre objetos.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede resultar en notificaciones excesivas e innecesarias a los observadores, especialmente si los cambios son frecuentes o si la actualización de los observadores es costosa en términos de rendimiento.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La gestión de la suscripción y cancelación de suscripciones debe manejarse con cuidado para evitar fugas de memoria o referencias a objetos que ya no deberían ser notificados.</p>
   </li>

   <li>
      <p style="text-align:justify;">Puede ser difícil depurar y rastrear el flujo de ejecución en sistemas complejos debido a la naturaleza dinámica de las notificaciones y la falta de una secuencia de llamadas directa.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Interfaces de usuario (UI):</strong> Para actualizar automáticamente los elementos de la interfaz de usuario en respuesta a cambios en el modelo de datos, como actualizar listas de elementos, gráficos o indicadores de estado.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de eventos en aplicaciones web:</strong> En el desarrollo de aplicaciones web modernas, para reaccionar a eventos del lado del cliente, como clics de botones, ingreso de datos, cambios en formularios, actualización reactiva de la UI, entre otros.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Aplicaciones de trading financiero:</strong> Para notificar a los clientes sobre cambios en los precios de las acciones, tasas de interés o indicadores de mercado, permitiendo reacciones rápidas a condiciones de mercado dinámicas.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Sistemas de monitoreo y alertas:</strong> En aplicaciones de monitoreo de redes, sistemas o aplicaciones que necesitan notificar a varios componentes o usuarios sobre eventos críticos, cambios de estado o alertas de seguridad.</p>
   </li>
</ul>