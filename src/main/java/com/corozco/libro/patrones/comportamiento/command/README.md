<h1 style="text-align:center;"> 
    <strong>Command (Comando)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Command</strong> encapsula los detalles de una solicitud (quién la recibirá y como la llevará a cabo) en un objeto denominado comando. De esta forma, cualquier objeto que necesite realizar la solicitud sólo debe preocuparse por ejecutar el comando. Como resultado, el invocador queda desacoplado de receptor.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
La mejor forma de entender el patrón es mediante una analogía: una universidad (cliente) se encarga de determinar cuales son los pasos que se deben seguir para procesar una solicitud (comando), y quién debe realizarlos (receptor). En general, la universidad define los protocolos y responsables para solicitudes de homologación, cancelación de cursos, exención de requisitos, entre otros. De esta manera, un estudiante (invocador) solo debe disparar el comando, p. ej.: "necesito cancelar el curso X", ignorando por completo que pasa después, y preocupándose solo por la respuesta de la solicitud (aceptación o rechazo). Gracias a ello, la universidad puede cambiar los mecanismos para la gestión de las solicitudes de manera completamente transparente para el estudiante.
</p>

<p style="text-align:justify;">
Volviendo al problema, el patrón <strong>Command</strong> es útil cuando existen escenarios en los cuales el invocador no necesita conocer como se lleva a cabo una operación. Para lograrlo, los comandos definen acciones genéricas que pueden ser configuradas de acuerdo con la necesidad del negocio. Además, el patrón habilita la posibilidad de crear cadenas de comandos que pueden ser ejecutados a demanda, ocultando los detalles de su implementación a clientes externos.
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> A continuación, se presenta el modelo propuesto por el GoF, dado que sigue siendo acertado y relevante. No obstante, mas adelante se propone una nueva versión con un ajuste sobre la propuesta original. El modelo propone un <strong>Invocador</strong> que almacena una estructura de datos con todos los posibles comandos que puede ejecutar. Por otro lado, la clase <strong>Cliente</strong> tiene la responsabilidad de determinar cual es el <strong>Receptor</strong> que se debe asociar a cada <strong>Comando</strong>.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/command_original.png" width="70%" height="auto" alt="">
</p>

<p style="text-align:justify;"> Sin embargo, el modelo tiene un problema conceptual que parece trivial pero puede causar confusión. Un lector se podría preguntar lo siguiente: "¿Tiene sentido que un cliente no tenga una relación con el invocador y si la tenga con el receptor?" El pensamiento natural sería que un cliente inicia una acción; no obstante, en el contexto del patrón pueden existir dos clientes: (ii) el "cliente" usual que se encarga de asociar el receptor a un comando (<strong>ClienteReceptor</strong>), y (ii) un cliente que puede disparar los comandos a través del invocador (incluso el invocador puede ser un cliente por si mismo). La confusión surge cuando se piensa que el cliente descrito en el modelo original es quién ejecuta el comando. Sin embargo, el cliente que asocia al receptor no está obligado a conocer al invocador; de este modo, el cliente que realiza las invocaciones puede ser un proceso, tarea o usuario final completamente diferente. Distintos ejemplos en la literatura y en la práctica asumen que el invocador y receptor son el mismo (cuando en realidad pueden ser dos procesos independientes.</p>

<p style="text-align:justify;"> A partir de lo anterior, en este libro se presenta una aproximación con un ligero ajuste para ayudar al lector a comprender el patrón con mayor facilidad (ver la siguiente figura). En esencia, la clase <strong>ClienteReceptor</strong> determina cual es el <strong>Receptor</strong> asociado a cada comando. La interfaz <strong>IComando</strong> define el contrato para la implementación de cada comando específico. La clase <strong>Invocador</strong> contiene las instancias de los comandos necesarios para acciones previamente definidas. Finalmente, un <strong>ClienteInvocador</strong> puede ejecutar los comandos que provee el <strong>Invocador</strong>.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/command.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Invocador:</strong> Clase que tiene el conjunto de comandos que un cliente puede ejecutar.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>IComando:</strong> Interfaz que declara la operación necesaria para ejecutar un comando concreto.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>Comando:</strong> Subclase que conoce al receptor y determina las operaciones que debe realizar.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>Receptor:</strong> Clase que realiza la acción solicitada por el invocador.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>ClienteReceptor:</strong> Clase encargada de asociar el receptor a un comando particular.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>ClienteInvocador:</strong> Clase que tiene la capacidad de ejecutar comandos.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Permite desacoplar el objeto que emite una solicitud del objeto que la ejecuta, aumentando la flexibilidad y la reutilización de ambos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la implementación de operaciones complejas como deshacer/rehacer, al mantener un historial de comandos ejecutados que se pueden revertir o repetir.</p>
   </li>

   <li>
      <p style="text-align:justify;">Soporta la implementación de colas de solicitudes y su ejecución diferida, permitiendo programar y ejecutar comandos en momentos específicos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite la agrupación de comandos para crear operaciones compuestas, mejorando la organización del código y promoviendo su reutilización.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede aumentar la complejidad del código, especialmente en aplicaciones simples donde el uso directo de las operaciones podría ser más claro y directo.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La implementación de múltiples comandos y su gestión puede llevar a un aumento en el número de clases e interfaces, complicando la estructura del proyecto.</p>
   </li>

   <li>
      <p style="text-align:justify;">La gestión del historial de comandos, especialmente para operaciones deshacer/rehacer, puede ser compleja y consumir recursos significativos.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Interfaces de usuario (UI):</strong> Para manejar acciones del usuario como clics de botones o selecciones de menú, donde cada acción puede ser encapsulada en un comando concreto.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Operaciones deshacer/rehacer:</strong> En editores de texto, gráficos o aplicaciones de modelado, donde se requiere que las acciones del usuario puedan ser revertidas o repetidas.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Colas de trabajo y ejecución de tareas en segundo plano:</strong> En aplicaciones que necesitan programar tareas para su ejecución posterior, gestionando colas de comandos a ser ejecutados en momentos específicos o bajo ciertas condiciones.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Transacciones:</strong> En sistemas que requieren ejecutar grupos de operaciones atómicamente, permitiendo agrupar comandos en una transacción que puede ser completada o revertida en su totalidad.</p>
   </li>
</ul>