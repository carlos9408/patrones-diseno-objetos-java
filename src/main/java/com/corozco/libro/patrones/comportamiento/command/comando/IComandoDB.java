package com.corozco.libro.patrones.comportamiento.command.comando;

public interface IComandoDB {
    void ejecutar(String sql);
}
