package com.corozco.libro.patrones.comportamiento.strategy.algoritmo;

public interface IAlgoritmoOrdenamiento {
    <T> void ordenar(T[] arr);
}
