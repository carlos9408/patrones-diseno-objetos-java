<h1 style="text-align:center;"> 
    <strong>Iterator (Iterador)</strong>
</h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Iterator</strong> provee un mecanismo para recorrer los elementos de un objeto de forma secuencial sin exponer su estructura interna.</p>

<h2><strong>Motivación</strong><br></h2>

<p style="text-align:justify;">
El patrón <strong>Iterator</strong> permite encapsular la lógica necesaria para recorrer de forma secuencial un objeto que pueda ser descrito como una estructura de datos. El paradigma orientado a objetos provee diferentes estructuras de datos (arreglos, listas, colas, pilas, entre otros) Sin embargo, el patrón <strong>Iterator</strong> se extiende a cualquier objeto que pueda ser representado como un iterable.
</p>


<p style="text-align:center;">
    <img src="../../../../../../../resources/images/comportamiento/iterator.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La interfaz <strong>Iterable</strong> declara el contrato para retornar un <strong>Iterador</strong> al cliente a través de un <strong>IterableConcreto</strong>. El <strong>IteradorConcreto</strong> implementa las operaciones necesarias para recorrer el <strong>IterableConcreto</strong> de interés.</p>

<p style="text-align:justify;"> Diferentes autores usan la designación <strong>Agregado</strong> cuando se refieren a la interfaz encargada de delegar la instanciación del <strong>Iterador</strong>. Sin embargo, en este libro se usa el término <strong>Iterable</strong>, es más natural pensar que una estructura "es" <strong>Iterable</strong> a pensar que "es un" <strong>Agregado</strong>.</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Iterador:</strong> Interfaz que define las operaciones para que un objeto se pueda recorrer secuencialmente.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>IteradorConcreto:</strong> Subclase que implementa el comportamiento definido por el iterador.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>Iterable:</strong> Interfaz que habilita a una clase para que se comporte como un iterador.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>IterableConcreto:</strong> Subclase que determina el iterador concreto designado por el usuario</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Facilita la navegación por elementos de una colección sin conocer su implementación interna, promoviendo el principio de encapsulamiento.</p>
   </li>

   <li>
      <p style="text-align:justify;">Proporciona una interfaz uniforme para recorrer diferentes estructuras de datos, lo que permite cambiar las estructuras de datos subyacentes sin afectar el código que las recorre.</p>
   </li>

   <li>
      <p style="text-align:justify;">Soporta variaciones en la iteración, como recorridos en diferentes direcciones o aplicando filtros, sin necesidad de cambiar la colección.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite el acceso simultáneo a una colección desde múltiples consumidores, cada uno con su propio estado de iteración, sin interferencia entre ellos.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede introducir una complejidad adicional y sobrecarga si la iteración es muy simple o si solo se necesita acceso secuencial.</p>
   </li>   

   <li>
      <p style="text-align:justify;">En algunas implementaciones, mantener el estado del iterador puede consumir recursos adicionales, especialmente para iteradores complejos o para grandes colecciones.</p>
   </li>

   <li>
      <p style="text-align:justify;">El uso incorrecto de iteradores, como modificar la colección mientras se está iterando, puede causar errores difíciles de rastrear.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Navegación en colecciones de datos:</strong> Para acceder y procesar elementos en listas, árboles, mapas y otras estructuras de datos, donde cada estructura proporciona su propio iterador específico.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Operaciones de filtrado y transformación:</strong> En bibliotecas de manipulación de datos, donde los iteradores pueden ser encadenados para aplicar múltiples transformaciones o filtros a los elementos de una colección.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Interfaces de usuario con colecciones de elementos:</strong> Para recorrer y mostrar elementos, como en interfaces de usuario que listan mensajes de correo, archivos en un directorio o ítems en un menú.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Integración con el patrón Composite:</strong> Para iterar uniformemente sobre objetos compuestos y sus elementos, tratando colecciones de objetos y objetos individuales de manera transparente.</p>
   </li>
</ul>