package com.corozco.libro.patrones.estructural.decorator.dominio;

public class SeguroBasico extends Seguro {

    public SeguroBasico(String seguro_base, int costo, boolean vitalicio) {
        super(seguro_base, costo, vitalicio);
    }
}
