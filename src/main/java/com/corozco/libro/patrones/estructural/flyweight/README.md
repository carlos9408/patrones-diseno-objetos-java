<h1 style="text-align:center;"> <strong>Flyweight (Peso Ligero)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Flyweight</strong> permite compartir el estado de uno o varios objetos que tienen información idéntica.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Flyweight</strong> es eficaz para optimizar el consumo de memoria RAM en la aplicación, especialmente cuando se manejan múltiples objetos que contienen datos idénticos. Para lograrlo, el patrón propone definir una estructura que tiene la capacidad de almacenar la información común entre objetos, mejorando el rendimiento general de una aplicación.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La clase <strong>Fabrica</strong> almacena los objetos ligeros y provee las operaciones necesarias para instanciar un nuevo objeto ligero en caso de ser necesario. La clase <strong>ObjetoLigero</strong>representa el estado abstracto de un objeto concreto. El <strong>ObjetoLigeroCompartido</strong> almacena la información de una instancia compartida entre múltiples objetos en la aplicación. El <strong>ObjetoLigeroNoCompartido</strong> es un objeto concreto que por algún motivo no necesita ser compartido en el contexto de la solución. Finalmente, los objetos compartidos y no compartidos contienen un estado intrínseco y extrínseco.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/flyweight.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Fabrica:</strong> Clase que almacena los objetos ligeros y provee la lógica para agregar o consultar su información.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ObjetoLigero:</strong> Clase que encapsula el detalle de un objeto ligero.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ObjetoLigeroCompartido:</strong> Subclase que contiene el estado intrínseco de un objeto ligero compartido.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>ObjetoLigeroNoCompartido:</strong> Subclase que representa un objeto ligero que no es compartido.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Reduce la cantidad de memoria y recursos utilizados al compartir estados comunes entre múltiples objetos, lo cual es crítico en aplicaciones con un gran número de instancias de objetos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Mejora el rendimiento de las aplicaciones en entornos con limitaciones de recursos al disminuir la carga sobre el sistema de gestión de memoria.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la escalabilidad de la aplicación al permitir la creación de una gran cantidad de objetos sin un aumento significativo en el consumo de memoria.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite una gestión más eficiente de los estados compartidos de los objetos, centralizando su control y actualización.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">La complejidad del diseño puede aumentar, ya que se requiere un cuidado especial en la gestión de los estados compartidos y los estados específicos de los objetos.</p>
   </li>   

   <li>
      <p style="text-align:justify;">Puede ser desafiante para los desarrolladores mantener un equilibrio entre eficiencia y simplicidad, especialmente en la implementación de la lógica para distinguir entre estados intrínsecos y extrínsecos.</p>
   </li>

   <li>
      <p style="text-align:justify;">El patrón puede introducir una sobrecarga adicional en tiempo de ejecución debido a la necesidad de gestionar el acceso y la actualización de los estados compartidos de manera eficiente.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de particulas en videojuegos:</strong> Para representar y gestionar miles de partículas (como fuego, humo, o chispas) de manera eficiente, compartiendo el estado de las partículas para reducir el consumo de memoria.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Gestión de documentos de texto:</strong> En editores de texto o procesadores de palabras, para compartir los formatos de caracteres comunes, como fuentes y tamaños, entre diferentes partes del documento.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Aplicaciones de renderizado gráfico:</strong> Para optimizar la representación de objetos gráficos en aplicaciones CAD o juegos, donde muchos objetos pueden compartir geometrías, texturas o sombras similares.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Desarrollo de aplicaciones web:</strong> En la gestión de objetos DOM para sitios web dinámicos, donde se pueden compartir estilos, comportamientos o atributos entre elementos similares para reducir la huella de memoria del navegador.</p>
   </li>
</ul>