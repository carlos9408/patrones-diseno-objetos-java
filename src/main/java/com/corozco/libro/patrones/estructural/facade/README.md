<h1 style="text-align:center;"> <strong>Facade (Fachada)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Facade</strong> simplifica el acceso a un subsistema externo a través de una interfaz común que encapsula el comportamiento interno del subsistema, ocultando los detalles internos sobre como opera.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> En ocasiones, un sistema necesita utilizar los recursos y/o servicios expuestos por módulos o funcionalidades complejas. El patrón <strong>Facade</strong> provee una abstracción que encapsula las operaciones de cada susbistema y oculta los detalles de su implementación.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;">La interfaz <strong>IFachada</strong> encapsula el acceso a uno o varios subsistemas. La subclase <strong>FachadaImpl</strong> ejecuta las operaciones expuestas por cada subsistema.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/facade.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IFachada:</strong> Contrato que expone un punto de entrada para que un cliente realice operaciones sin conocer quién las lleva a cabo.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>FachadaImpl:</strong> Subclase que instancia y utiliza las operaciones expuestas por uno o varios subsistemas.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Subsistema:</strong> Subclase que determina la nueva funcionalidad del <strong>ComponenteConcreto</strong>.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>DecoradorConcreto:</strong> Módulo, paquete o sistema invocado por la fachada.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Simplifica la comunicación con un subsistema complejo, ofreciendo a los clientes una vista simplificada y reduciendo la complejidad de las interacciones.</p>
   </li>

   <li>
      <p style="text-align:justify;">Aísla a los clientes de los componentes del subsistema, promoviendo un menor acoplamiento y mejorando la modularidad del sistema.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la división del sistema en capas, haciendo más fácil gestionar dependencias y organizar el código de manera coherente.</p>
   </li>

   <li>
      <p style="text-align:justify;">Proporciona un punto de entrada único para el subsistema, ayudando a centralizar la gestión de tareas comunes.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede convertirse en un objeto "Dios" al asumir demasiadas responsabilidades, lo que puede llevar a una violación del principio de responsabilidad única si la fachada se sobrecarga.</p>
   </li>   

   <li>
      <p style="text-align:justify;">El uso inadecuado puede llevar a los clientes a depender excesivamente de la fachada, limitando el uso de las funcionalidades avanzadas disponibles en el subsistema.</p>
   </li>

   <li>
      <p style="text-align:justify;">Algunas operaciones pueden requerir tareas personalizadas que la fachada no puede proporcionar, forzando a los clientes a interactuar directamente con el subsistema.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de librerías complejas:</strong> Para proporcionar una interfaz simplificada a conjuntos de librerías o frameworks complejos, facilitando su uso y configuración.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>APIs de backend para frontend (BFF):</strong> En el desarrollo de aplicaciones web o móviles, para ofrecer una interfaz simplificada a los servicios de backend, adaptando los datos y operaciones a las necesidades específicas del frontend.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Sistemas legados:</strong> Para encapsular y simplificar la interacción con sistemas legados o de terceros, permitiendo una integración más limpia y fácil de mantener dentro de aplicaciones modernas.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Controladores de dispositivos:</strong> En sistemas embebidos o aplicaciones que interactúan con hardware, para ofrecer una interfaz simple hacia operaciones complejas de control de dispositivos o comunicación con periféricos.</p>
   </li>
</ul>