package com.corozco.libro.patrones.estructural.bridge.arquitectura;

public class ArquitecturaX32 extends Arquitectura {

    public ArquitecturaX32(String nombre, String descripcion) {
        super(nombre, descripcion);
    }
}
