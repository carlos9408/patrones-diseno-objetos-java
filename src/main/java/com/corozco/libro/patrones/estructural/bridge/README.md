<h1 style="text-align:center;"> <strong>Bridge (Puente)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> De acuerdo con la definición propuesta por el GoF que ha sido replicada por otros autores con el paso de los años, el patrón <strong>Bridge</strong> tiene la función de separar una abstracción de su implementación con el propósito de permitir que ambos evolucionen de forma independiente. Sin embargo, esta definición (aunque es concisa) tiene algunos problemas, entre ellos: (i) es demasiado genérica, (ii) no provee suficiente claridad, y (iii) no tiene nada que ver con su nombre, lo cual genera confusión.</p>

<p style="text-align:justify;">Con el animo de establecer un panorama más claro, en este libro se presenta una definición con mayor nivel de detalle. Para lograrlo, primero se presentan los siguientes conceptos:</p>

<ul>
   <li>
      <p style="text-align:justify;"><strong>Abstracción:</strong> En el contexto del patrón de diseño, una abstracción es un contrato que define operaciones sin cuerpo, cuya implementación es realizada por las subclases que tenga asociadas. En otras palabras, una abstracción es una interfaz o una clase abstracta.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Implementación:</strong> Es la subclase que materializa una o varias acciones definidas por una abstracción.</p>
   </li>
</ul>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Bridge</strong> busca evitar las jerarquías de clases con mas de un nivel de profundidad aplicando el principio \quotes{composición sobre herencia}, eliminando la jerarquía de segundo nivel mediante la definición de una estructura adicional que agrupe dicha jerarquía, y que es asociada a la abstracción a través de una relación de composición.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;">Hasta ahora, presentar los patrones ha sido una tarea relativamente sencilla debido a que se enfocan a solucionar un problema intuitivo o fácil de comprender. Sin embargo, en este caso se presenta un modelo que describe el problema que se busca abordar antes de presentar la solución. De acuerdo con el diagrama presentado en la siguiente figura, el problema radica en la existencia de implementaciones en el segundo nivel de la jerarquía, las cuales pertenecen a una familia de clases particular.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/bridge_problema.png" width="70%" height="auto" alt="">
</p>

<p style="text-align:justify;">En la siguiente figura, se presenta la solución al modelo presentado en la figura anterior aplicando el patrón <strong>Bridge</strong>. De acuerdo con el modelo, la <strong>Abstraccion</strong> representa la superclase de una jerarquía cuya implementación está dada por cada <strong>AbstraccionRefinada</strong>. La jerarquía de segundo nivel se convierte en una relación de composición a través de la declaración de una <strong>Implementacion</strong> que puede tener implementaciones concretas.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/bridge.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Abstraccion:</strong> Clase o contrato que representa la raíz de una jerarquía de clases.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>AbstraccionRefinada:</strong> Implementación de la abstracción que conforma las relaciones de primer nivel.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Implementacion:</strong> Clase o contrato que representa un subtipo de una abstracción refinada.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ImplementacionConcreta:</strong> Subclase que implementa las operaciones realizadas por una implementación.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Mejora la extensibilidad del sistema al permitir que dos familias de clases puedan evolucionar de manera independiente.</p>
   </li>

   <li>
      <p style="text-align:justify;">Promueve el principio de composición sobre la herencia, favoreciendo una estructura de código más flexible y mantenible.</p>
   </li>

   <li>
      <p style="text-align:justify;">Reduce la complejidad del código al separar una interfaz de su implementación, facilitando una mayor claridad en el diseño del sistema.</p>
   </li>

   <li>
      <p style="text-align:justify;">Incrementa la posibilidad de reutilización del código, ya que las implementaciones pueden ser intercambiadas o reutilizadas por diferentes abstracciones.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">La complejidad de la implementación puede aumentar, ya que se requiere una mayor planificación y comprensión del patrón para aplicarlo efectivamente.</p>
   </li>   

   <li>
      <p style="text-align:justify;">Puede ser más difícil de razonar sobre el código, especialmente para nuevos desarrolladores no familiarizados con el patrón, debido a las capas adicionales de abstracción.</p>
   </li>

   <li>
      <p style="text-align:justify;">En casos donde no existen muchas variaciones en las implementaciones o no se requiere cambiarlas en tiempo de ejecución, el uso del patrón puede ser innecesario.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Interfaces de usuario multiplataforma:</strong> Permite desarrollar aplicaciones que funcionen en múltiples plataformas, con una abstracción común de la interfaz de usuario que se conecta a implementaciones específicas de la plataforma.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Soporte para diferentes formatos de archivos:</strong> Facilita la manipulación de diferentes formatos de archivos mediante la definición de una abstracción para operaciones comunes que se implementan de manera específica para cada formato.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Gestión de recursos en videojuegos:</strong> Permite abstraer el manejo de recursos gráficos o de audio de sus implementaciones concretas, facilitando el cambio entre diferentes bibliotecas de gráficos o audio.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Aplicaciones con necesidades de adaptación a dispositivos:</strong> Ofrece una solución para aplicaciones que necesitan adaptarse a dispositivos con capacidades diferentes, como teléfonos móviles, tabletas y computadoras de escritorio, mediante abstracciones que se pueden vincular a implementaciones específicas del dispositivo.</p>
   </li>
</ul>