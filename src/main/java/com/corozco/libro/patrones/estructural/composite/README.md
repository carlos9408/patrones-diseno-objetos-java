<h1 style="text-align:center;"> <strong>Composite (Compuesto)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> A diferencia de la mayoría de patrones presentados en el libro, el patrón <strong>Composite</strong> tiene un propósito muy específico: crear familias de objetos que pueden ser representadas mediante una estructura en forma de árbol a través del uso de composición.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Bridge</strong> es efectivo cuando existe un problema que puede ser solucionado modelando un árbol que cumple la condición todo-parte, en la cual cada nodo
puede ser considerado la parte más simple del árbol ---una hoja--- o ser un subárbol.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;">La clase <strong>Componente</strong> representa la raíz del árbol. Cada <strong>Compuesto</strong> almacena una estructura de datos de componentes. La clase <strong>Hoja</strong> representa el estado más sencillo que puede tener un <strong>Componente</strong>, siendo incapaz de asociar otros componentes.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/composite.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Componente:</strong> Estructura que puede ser: (i) la raiz del árbol, (ii) la raíz de un subárbol, o (iii) una hoja.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Compuesto:</strong> Estructura que representa un subárbol. El compuesto almacena un arreglo de componentes (subárboles u hojas).</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Hoja:</strong> La forma más sencilla que puede tomar un componente. Las hojas no tienen hijos.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Facilita la adición de nuevos tipos de componentes, ya que los cambios se aíslan dentro de la jerarquía existente, promoviendo la extensibilidad.</p>
   </li>

   <li>
      <p style="text-align:justify;">Mejora la claridad y la estructura del diseño al reflejar jerarquías "todo-parte" en la organización de los objetos, haciendo que el modelo de dominio sea más comprensible.</p>
   </li>

   <li>
      <p style="text-align:justify;">Proporciona flexibilidad en el diseño, ya que se pueden construir estructuras complejas a partir de componentes simples y compuestos que se pueden modificar en tiempo de ejecución.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Todos los componentes del árbol deben compartir una interfaz común, dificultando la extensión de la solución en diferentes contextos.</p>
   </li>   

   <li>
      <p style="text-align:justify;">Diseñar la jerarquía de clases correctamente puede ser complejo, especialmente cuando se desea añadir nuevas operaciones a la clase <strong>Composite</strong> que no son aplicables a los componentes individuales.</p>
   </li>

   <li>
      <p style="text-align:justify;">El patrón puede llevar a un diseño excesivamente generalizado, haciendo más difícil restringir el tipo de objetos que pueden formar parte de la composición.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Sistemas de archivos:</strong> Modela la estructura de archivos y carpetas, donde tanto archivos individuales como carpetas (que pueden contener otros archivos o carpetas) son tratados como elementos del sistema de archivos.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Interfaces gráficas de usuario (GUI):</strong> Organiza componentes de una interfaz, como botones, cuadros de texto y paneles, donde los paneles pueden contener otros componentes, incluidos otros paneles, formando una jerarquía visual.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Gestión de organizaciones o estructuras departamentales:</strong> Representa estructuras organizativas, donde tanto departamentos individuales como agrupaciones de estos (por ejemplo, divisiones o equipos) se manejan uniformemente.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Procesamiento de gráficos vectoriales:</strong> Permite la composición de gráficos complejos a partir de primitivas simples (como líneas, círculos, y polígonos) y composiciones de estos para formar dibujos más complejos.</p>
   </li>
</ul>