<h1 style="text-align:center;"> <strong>Decorator (Decorador)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Decorator</strong> permite extender el alcance de una clase sin modificarla.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> En general, muchos profesionales se ven tentados a extender nuevas funcionalidades mediante el uso de herencia. Sin embargo, la herencia trae consigo aspectos relacionados con: (i) replicar la nueva funcionalidad a todas las clases hijas, (ii) sobrescribir operaciones previamente definidas, (iii) crear acoplamiento adicional en las clases existentes, entre otros. El patrón <strong>Decorator</strong> busca evitar el uso de herencia directa mediante la definición de una clase concreta que extiende la funcionalidad del objeto utilizando composición.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;">La interfaz <strong>IComponente</strong> delega la definición de una nueva característica o funcionalidad de interés para el sistema a la clase <strong>Decorador</strong> a través de un <strong>DecoradorConcreto</strong> Por otro lado, un <strong>ComponenteConcreto</strong> representa un elemento funcional en la aplicación que no necesita asociar nuevas características.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/decorator.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IComponente:</strong> Contrato que declara la operación necesaria para decorar un objeto.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ComponenteConcreto:</strong> Subclase que representa el componente que se va a decorar.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Decorador:</strong> Subclase que determina la nueva funcionalidad del <strong>ComponenteConcreto</strong>.</p>
   </li>
    <li>
      <p style="text-align:justify;"><strong>DecoradorConcreto:</strong> Subclase que realiza las acciones asociadas con la nueva operación del componente.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Aporta una alternativa flexible a la herencia para extender una funcionalidad, permitiendo añadir responsabilidades a los objetos de forma dinámica.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita la combinación de comportamientos al permitir apilar decoradores de manera dinámica, ofreciendo una gran variedad de comportamientos compuestos.</p>
   </li>

   <li>
      <p style="text-align:justify;">Mejora la extensibilidad y la reutilización al separar las preocupaciones en diferentes clases, promoviendo un diseño de software más limpio y modular.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite modificar el comportamiento de los objetos en tiempo de ejecución, lo cual es especialmente útil en sistemas donde los cambios en el comportamiento deben ser altamente flexibles y configurables.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede resultar en diseños con una gran cantidad de pequeñas clases, lo que puede complicar la comprensión y el mantenimiento del código.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La implementación de decoradores puede complicarse cuando se requiere acceso a los atributos privados del objeto decorado, lo cual puede violar el principio de encapsulamiento.</p>
   </li>

   <li>
      <p style="text-align:justify;">La utilización excesiva del patrón puede llevar a una estructura de objetos profundamente anidada, lo que puede afectar el rendimiento y complicar el seguimiento de las operaciones.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Interfaces gráficas de usuario (GUI):</strong> Para añadir comportamientos dinámicamente a elementos de la interfaz, como agregar bordes, desplazamiento o sombreado a los componentes sin modificar el código existente.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Sistemas de streaming de datos:</strong> Para encapsular y extender flujos de entrada y salida, agregando funcionalidades como cifrado, compresión y descompresión de datos de manera transparente.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Validación de datos:</strong> Para construir cadenas de validación complejas aplicando diferentes criterios de validación sobre objetos de datos, permitiendo una fácil extensión o modificación de las reglas de validación.</p>
   </li>
</ul>