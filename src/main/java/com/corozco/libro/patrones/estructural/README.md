<h1 style="text-align:center;"> <strong>Patrones Estructurales</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> 
    Los patrones estructurales tienen la tarea de definir la lógica necesaria para que un conjunto de clases u objetos puedan interactuar entre sí, combinarse y formar estructuras más complejas. El uso de patrones estructurales permite que un sistema agregue nuevas funcionalidades de manera flexible manteniendo su integridad, aprovechando al máximo el uso de las relaciones entre clases (dependencia, asociación, agregación, composición y generalización). Gracias a ello, un sistema puede crecer de manera orgánica.
</p>

----

<h2><strong>Importancia</strong><br></h2>
<p style="text-align:justify;"> 
    Después de superar la etapa de creación de las estructuras y elementos necesarios para cumplir con los objetivos mínimos de funcionamiento de un sistema, se llega a un punto en el cual cada solución comienza a enfrentar desafíos relacionados con mantener grados de cohesión y acoplamiento adecuados. En este punto, es posible que el sistema comience a crecer y sea necesario agregar nuevas funcionalidades, crear canales de comunicación o incluso modificar la lógica existente (esto puede suceder por aspectos como: decisiones funcionales, necesidad del negocio, decisiones de diseño, entre otros). 
</p>

<p style="text-align:justify;"> 
    Considerando lo anterior, es responsabilidad del equipo garantizar que la solución no se degrade con el paso del tiempo. Para lograrlo, es necesario contar con un conjunto de herramientas que permitan extender y modificar las soluciones existentes, y así facilitar que el sistema escale de manera natural. 
</p>


----

<h2><strong>Listado de patrones</strong><br></h2>

<h3>- [Adapter (Adaptador)](adapter/README.md)</h3>
<h3>- [Bridge (Puente)](bridge/README.md)</h3>
<h3>- [Composite (Compuesto)](composite/README.md)</h3>
<h3>- [Decorator (Decorador)](decorator/README.md)</h3>
<h3>- [Facade (Fachada)](facade/README.mdd)</h3>
<h3>- [Flyweight (Peso Ligero)](flyweight/README.md)</h3>
<h3>- [Proxy (Intermediario)](proxy/README.md)</h3>


----