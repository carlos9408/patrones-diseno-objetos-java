package com.corozco.libro.patrones.estructural.facade.subsistemas;

public class ValidadorIntegracion {

    public void validarEstilo() {
        System.out.println("\tValidacion de estilo... exitoso");
    }

    public void construir() {
        System.out.println("\tConstruccion de proyecto en local... exitoso");
    }

    public void pruebasUnitarias() {
        System.out.println("\tPruebas unitarias... exitoso");
    }
}
