<h1 style="text-align:center;"> <strong>Proxy (Intermediario)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Proxy</strong> establece un intermediario que regula el acceso a recursos externos, actuando como una interfaz que reemplaza de manera temporal al objeto requerido por el cliente. Este enfoque permite realizar actividades complementarias, las cuales, por diversas razones, no requieren o no deben ser ejecutadas directamente por el objeto original. Un proxy puede ser clasificado de diferentes maneras, las cuales se mencionan a continuación:</p>

<ul>
   <li>
       <p style="text-align:justify;"><strong>Referencia inteligente:</strong> Instancia que sustituye la referencia de un objeto para realizar operaciones adicionales sin modificar el objeto original.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Proxy remoto:</strong> Instancia local de un objeto que existe fuera del contexto de la aplicación.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Proxy virtual:</strong> Instancia que retrasa la creación de objetos costosos en tiempo de ejecución.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Proxy de protección:</strong> Instancia que controla el acceso a un recurso u objeto.</p>
   </li>
</ul>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Proxy</strong> es útil cuando se debe limitar el acceso a objetos pesados, o cuando es necesario realizar procesos de autenticación y/o autorización para verificar que un cliente tiene privilegios para acceder a un recurso.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;"> La interfaz <strong>IObjetivo</strong> delega la invocación de una operación a la clase <strong>Proxy</strong>, que actúa como intermediario entre el cliente y la clase <strong>Objeto</strong>. El propósito del <strong>Proxy</strong> es ocultar la lógica definida por la clase <strong>Objeto</strong>.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/proxy.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>IObjetivo:</strong> Abstracción que oculta el objeto real al cliente.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Objeto:</strong> Clase que representa el recurso real que se quiere acceder.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Proxy:</strong> Intermediario entre el cliente y el objeto real.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Facilita el control sobre el objeto real, permitiendo gestionar su ciclo de vida o interceptar operaciones sospechosas antes de acceder a él.</p>
   </li>

   <li>
      <p style="text-align:justify;">Ofrece la posibilidad de añadir funcionalidades adicionales cuando un objeto es accedido, como operaciones de carga perezosa, registro (logging), o control de acceso.</p>
   </li>

   <li>
      <p style="text-align:justify;">Permite la representación de objetos que pueden estar en sistemas remotos, navegadores web o en entornos con restricciones de recursos, actuando como un intermediario local.</p>
   </li>

   <li>
      <p style="text-align:justify;">Mejora el rendimiento de la aplicación mediante técnicas como la carga perezosa o la inicialización bajo demanda, especialmente útil para objetos grandes o recursos intensivos.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<ul>
   <li>
      <p style="text-align:justify;">Puede introducir una latencia adicional al añadir una capa extra de indirección al acceder a un objeto, lo que podría afectar el rendimiento en operaciones críticas.</p>
   </li>   

   <li>
      <p style="text-align:justify;">La implementación de proxies puede complicar el diseño del sistema, especialmente si se abusa de su uso o se implementan de manera ineficiente.</p>
   </li>

   <li>
      <p style="text-align:justify;">Puede ser más difícil de depurar debido a la indirección adicional, ya que requiere seguir la lógica a través de múltiples capas de objetos <strong>Proxy</strong>.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
       <p style="text-align:justify;"><strong>Control de acceso:</strong> Para restringir el acceso a ciertos métodos o datos de un objeto, especialmente en entornos donde se requieren medidas de seguridad o validación de permisos.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Carga perezosa:</strong> En la gestión de recursos pesados o costosos de crear, el proxy puede instanciar estos objetos solo cuando se accede a ellos por primera vez, reduciendo el consumo de recursos.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Operaciones de registro y auditoría:</strong> Para interceptar llamadas a métodos específicos de un objeto y realizar registro o auditoría de las operaciones realizadas, proporcionando transparencia en el uso del objeto.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Servicios remotos:</strong> En sistemas distribuidos, donde se accede a objetos que residen en diferentes ubicaciones o servidores, el proxy actúa como un intermediario local para simplificar y gestionar la comunicación remota.</p>
   </li>
</ul>