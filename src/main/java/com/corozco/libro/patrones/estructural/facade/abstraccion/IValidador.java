package com.corozco.libro.patrones.estructural.facade.abstraccion;

public interface IValidador {

    void validar();
}
