<h1 style="text-align:center;"> <strong>Adapter (Adaptador)</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> El patrón <strong>Adapter</strong> se utiliza cuando es necesario crear una estructura homogénea para comunicar clases incompatibles.</p>

----

<h2><strong>Motivación</strong><br></h2>
<p style="text-align:justify;"> Es común que un sistema sea diseñado siguiendo un conjunto de reglas específicos de acuerdo con las necesidades del negocio. No obstante, otros sistemas podrían establecer sus propias operaciones para representar el mismo problema. En estos casos, es probable que la comunicación sea difícil (o incluso imposible); para abordar este problema, se propone un intermediario que provea un canal de comunicación entre los objetos y/o sistemas que no son compatibles.</p> 

----

<h2><strong>Modelo UML</strong><br></h2>

<p style="text-align:justify;">La interfaz <strong>IObjetivo</strong> abstrae los objetos incompatibles a una estructura común. La clase <strong>Adaptador</strong> contiene una referencia al objeto incompatible y ejecuta las operaciones necesarias para convertir un  <strong>ProductoIncompatible</strong> en un <strong>Producto</strong> conocido por el sistema.</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/estructural/adapter.png" width="70%" height="auto" alt="">
</p>

----

<h2><strong>Participantes</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Producto:</strong> Clase que conoce el sistema.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>ProductoIncompatible:</strong> Clase incompatible.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>IObjetivo:</strong> Contrato que centraliza las operaciones entre dos clases inicialmente incompatibles a través de una estructura homogénea.</p>
   </li>
   <li>
      <p style="text-align:justify;"><strong>Adaptador:</strong> Subclase que transforma las operaciones de un <strong>ProductoIncompatible</strong> a una estructura común.</p>
   </li>
</ul>

----

<h2><strong>Ventajas</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;">Permite la colaboración entre clases que no podrían trabajar juntas debido a interfaces incompatibles, aumentando la reutilización del código existente.</p>
   </li>

   <li>
      <p style="text-align:justify;">Proporciona una mayor flexibilidad en el diseño del software, permitiendo cambiar las interfaces de las clases existentes sin modificar el código que las utiliza.</p>
   </li>

   <li>
      <p style="text-align:justify;">Facilita el cambio o la extensión de las implementaciones de las clases adaptadas sin afectar a los clientes que dependen del adaptador.</p>
   </li>
</ul>

----

<h2><strong>Desventajas</strong><br></h2>

<p style="text-align:justify;"> De acuerdo con algunos críticos de este patrón, necesitar un adaptador es un indicador de fallos de diseño, funcionando como un parche que busca mitigar problemas existentes. No obstante, la decisión de utilizar un adaptador puede obedecer a motivos de incompatibilidad ajenos a la solución. A continuación, se presentan algunas desventajas conocidas de utilizar el patrón.</p>

<ul>
   <li>
      <p style="text-align:justify;">Puede aumentar la complejidad del código al introducir conjuntos adicionales de clases e interfaces, especialmente en sistemas grandes.</p>
   </li>

   <li>
      <p style="text-align:justify;">El uso excesivo de adaptadores puede resultar en un diseño de software menos claro, con muchas pequeñas clases e interfaces que pueden ser difíciles de seguir y mantener.</p>
   </li>

   <li>
      <p style="text-align:justify;">Potencialmente, se podrían generar problemas de rendimiento debido a la capa extra de abstracción introducida por el adaptador, especialmente cuando se trabaja con operaciones críticas.</p>
   </li>
</ul>

----

<h2><strong>Escenarios de aplicación</strong><br></h2>
<ul>
   <li>
      <p style="text-align:justify;"><strong>Integración de sistemas externos:</strong> acilita la comunicación entre la aplicación principal y sistemas externos o bibliotecas de terceros, adaptando las interfaces de estos últimos a lo que espera la aplicación.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Soporte para formatos de datos antiguos:</strong> Permite a las aplicaciones modernas trabajar con formatos de datos o APIs legadas, convirtiendo los datos antiguos a formatos más recientes sin alterar el código original.</p>
   </li>

   <li>
       <p style="text-align:justify;"><strong>Desarrollo de plugins y extensiones:</strong> Proporciona una manera de extender las funcionalidades de las aplicaciones permitiendo que nuevos plugins o extensiones se integren a través de adaptadores específicos.</p>
   </li>

   <li>
      <p style="text-align:justify;"><strong>Adaptación de interfaces de usuario:</strong> Permite reutilizar componentes de la interfaz de usuario que fueron diseñados para un tipo de sistema o framework en otro, adaptando los métodos de eventos y acciones sin cambiar el componente original.</p>
   </li>
</ul>