package com.corozco.libro.patrones.estructural.adapter.dominio;

public interface IEmpleado {

    String getDocumento();

    String getNombreCompleto();

    String getTelefono();
}
