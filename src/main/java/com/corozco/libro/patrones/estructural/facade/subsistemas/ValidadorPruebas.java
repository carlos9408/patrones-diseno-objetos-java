package com.corozco.libro.patrones.estructural.facade.subsistemas;

public class ValidadorPruebas {

    public void pruebasEstres() {
        System.out.println("\tPruebas de estres... exitoso");
    }

    public void pruebasHumo() {
        System.out.println("\tPruebas de humo... exitoso");
    }

    public void pruebasIntegracion() {
        System.out.println("\tPruebas de integración... exitoso");
    }
}
