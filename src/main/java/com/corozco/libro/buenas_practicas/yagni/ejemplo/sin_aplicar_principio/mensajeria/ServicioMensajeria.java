package com.corozco.libro.buenas_practicas.yagni.ejemplo.sin_aplicar_principio.mensajeria;

import com.corozco.libro.buenas_practicas.yagni.ejemplo.sin_aplicar_principio.Nota;

public interface ServicioMensajeria {
    void compartirNota(Nota nota);
}