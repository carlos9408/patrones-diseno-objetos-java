<h1 style="text-align:center;">Principio YAGNI (You Aren't Gonna Need It)</h1>

<hr>

<h4 style="text-align:center;"><em>"No construyas hoy lo que no vas a necesitar mañana".</em></h4>

<p style="text-align:justify;">
    El principio <strong>YAGNI</strong>, cuyo acrónimo significa <em>You Aren't Gonna Need It</em> ---traducido al español como <em>No lo vas a necesitar</em>---, es una guía que enfatiza la importancia de evitar la implementación de funcionalidades innecesarias en un proyecto de software.
</p>

<p style="text-align:justify;">
    El principio <strong>YAGNI</strong> fue mencionado por primera vez como una consecuencia de aplicar la metodología de <strong>Programación Extrema</strong> (XP), propuesta por Kent Beck (<a href="#Beck2000">Beck, 2000</a>). Su objetivo es promover la simplicidad y la eficiencia en el desarrollo de software, evitando la implementación de código y funcionalidades que no ofrezcan un valor inmediato al producto o al usuario final.
</p>

<p style="text-align:justify;">
    Implementar características que podrían ser necesarias en el futuro, pero que no tienen un requerimiento claro en el presente, puede llevar a una complejidad innecesaria, incrementando la posibilidad de introducir errores y dificultando el mantenimiento y la comprensión del código. Por otro lado, dedicar esfuerzos en artefactos y/o entregables que no aportan valor inmediato consume recursos —esfuerzo, tiempo y personas—, lo que supone un costo adicional que puede ser perjudicial para un proyecto.
</p>

<p style="text-align:justify;">
    En este sentido, el principio <strong>YAGNI</strong> recomienda que los equipos de trabajo se enfoquen en las funcionalidades mínimas requeridas para satisfacer las necesidades actuales de un cliente, evitando diseñar e implementar soluciones que probablemente no serán útiles en el corto o mediano plazo.
</p>

<h2>Referencias</h2>

* Beck, K. (2000). Extreme programming explained: Embrace change (1st ed.). Addison-Wesley Professional.