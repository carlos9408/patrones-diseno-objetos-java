package com.corozco.libro.buenas_practicas.yagni.ejemplo.sin_aplicar_principio.exporte;

import com.corozco.libro.buenas_practicas.yagni.ejemplo.sin_aplicar_principio.Nota;

public interface ServicioExporte {
    void exportarNota(Nota nota);
}
