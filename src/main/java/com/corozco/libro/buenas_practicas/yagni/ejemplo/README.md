<h1 style="text-align:center;"> <strong>Ejemplo: sistema de gestión de notas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Un cliente contrató a <em>nuestro desarrollador</em> para trabajar en una aplicación de notas. De acuerdo con la especificación actual del proyecto, se solicitó a <em>nuestro desarrollador</em> crear un módulo que permita realizar operaciones básicas sobre las notas ---crear, editar y eliminar---. Luego de terminar con el requerimiento, se espera que <em>nuestro desarrollador</em> continúe trabajando en otras tareas que ya están listas para su implementación.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio YAGNI</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/yagni/yagni_ejemplo_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio YAGNI</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/yagni/yagni_ejemplo_bien.png" width=70% height=70% alt="">
</p>