<h1 style="text-align:center;">Principios y buenas prácticas de diseño</h1>

<hr>

<p style="text-align:justify;">
    En el capítulo anterior se proporcionaron los fundamentos conceptuales necesarios para comprender el paradigma de desarrollo de software orientado a objetos. Además, se presentó al lector el uso de diagramas de clase como una técnica para representar las relaciones entre los componentes de una solución basada en este enfoque. En este sentido, se espera que en este punto el lector cuente con las herramientas básicas que le permitirán analizar problemas y proponer soluciones desde la perspectiva del diseño orientado a objetos. Sin embargo, con el propósito de extender la base teórica inicial que subyace a este paradigma, los autores consideran pertinente presentar al lector los principios de diseño y las buenas prácticas de desarrollo más reconocidos y aplicados en este contexto.
</p>

<p style="text-align:justify;">
    En el contexto del desarrollo de software, una buena práctica se puede definir de manera informal como una solución que ha demostrado funcionar una y otra vez hasta transformarse gradualmente en un estándar de facto para solucionar problemas específicos. Los principios y buenas prácticas proveen una hoja de ruta que facilita la implementación de sistemas que son más fáciles de entender y extender. Sin embargo, es importante entender que las buenas prácticas no son reglas universales; por ejemplo: el uso de comentarios en el código fuente es un tema de amplio debate. Los defensores de esta idea argumentan que se debe incluir tanta información como sea necesaria para entender una solución, mientras que algunos detractores opinan que un código con comentarios excesivos refleja una solución que no es lo suficientemente clara por sí misma.
</p>

<p style="text-align:justify;">
    De este modo, el lector debe entender que las llamadas <em>buenas prácticas</em> no son verdades absolutas o principios inmutables tallados en piedra. En este capítulo se presentarán algunos de los principios más conocidos que son considerados buenas prácticas para desarrollar soluciones de software. Estos principios, respaldados por años de experiencia y adopción en la industria, son considerados una guía que promueve el desarrollo de soluciones con altos estándares de calidad.
</p>

<p style="text-align:justify;">
    <strong>Nota:</strong> En este capítulo se presentarán ejemplos con clases o métodos sin cuerpo. El propósito de las siguientes secciones es hacer énfasis en la base conceptual de cada ejemplo, dejando de lado implementaciones concretas.
</p>