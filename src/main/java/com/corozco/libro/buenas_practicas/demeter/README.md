<h1 style="text-align:center;">Ley de Demeter</h1>

<hr>

<h4 style="text-align:center;"><em>"No hables con extraños".</em></h4>

<p style="text-align:justify;">
    La <strong>Ley de Demeter</strong>, también conocida como <strong>principio de menor conocimiento</strong> o <strong>principio de menos acoplamiento</strong>, es una directriz de diseño de software que promueve la reducción de la dependencia entre diferentes módulos o componentes de un sistema. Este principio fue propuesto por primera vez en <strong>1987</strong> por Ian Holland en el contexto de un proyecto de programación orientada a objetos implementado en la Universidad de Northeastern (<a href="#Lieberherr1989">Lieberherr, 1989</a>).
</p>

<p style="text-align:justify;">
    La ley de Demeter establece que un objeto o clase sólo debería interactuar con los objetos que están directamente relacionados con él. En otras palabras, una unidad de software —como una clase o un método— no debería conocer ni interactuar directamente con los detalles internos de otras unidades, sino únicamente con los suyos propios y con los de sus colaboradores inmediatos (ver la <a href="#demeter:ley_demeter_generico">Figura 1</a>).
</p>


<div style="text-align:center;">
    <figure style="display:inline-block;">
        <img src="../../../../../../resources/images/principios/demeter/ley_demeter_generico.png" style="width:80%;" alt="Ley de Demeter - estructura genérica">
        <figcaption><strong>Figura 1:</strong> Ley de Demeter - estructura genérica.</figcaption>
        <a name="demeter:ley_demeter_generico"></a>
    </figure>
</div>

<p style="text-align:justify;">
    Para entender intuitivamente la ley de Demeter, considera el diagrama de clases presentado en la <a href="#demeter:ley_demeter_mal">Figura 2</a>. Según el modelo, la clase <strong>A</strong> puede crear una instancia de la clase <strong>B</strong>, que a su vez puede crear una instancia de la clase <strong>C</strong>. De acuerdo con la ley de Demeter, las clases <strong>A</strong> y <strong>C</strong> no se "conocen"; es decir, una instancia de la clase <strong>A</strong> no debería poder acceder directamente a los métodos o atributos que expone <strong>C</strong>.
</p>

<div style="text-align:center;">
    <figure style="display:inline-block;">
    <img src="../../../../../../resources/images/principios/demeter/ley_demeter_mal.png" style="width:100%;" alt="Ley de Demeter - modelo incorrecto">
    <figcaption><strong>Figura 2:</strong> Ley de Demeter - modelo incorrecto.</figcaption>
    <a name="demeter:ley_demeter_mal"></a>
</figure>
</div>

<p style="text-align:justify;">
    Al aplicar este principio, se espera que la clase <strong>B</strong> encapsule la lógica necesaria para que la clase <strong>A</strong> pueda acceder al atributo de la clase <strong>C</strong> (ver la <a href="#demeter:ley_demeter_bien">Figura 3</a>).
</p>

<div style="text-align:center;">
    <figure style="display:inline-block;">
        <img src="../../../../../../resources/images/principios/demeter/ley_demeter_bien.png" style="width:100%;" alt="Ley de Demeter - modelo correcto">
        <figcaption><strong>Figura 3:</strong> Ley de Demeter - modelo correcto.</figcaption>
        <a name="demeter:ley_demeter_bien"></a>
    </figure>
</div>

<hr>

<h2>Referencias</h2>

* Lieberherr, K. J., Holland, I., & Riel, A. J. (1988). Object-oriented programming: An objective sense of style.
  SIGPLAN Notices, 23(11), 323-334. https://doi.org/10.1145/62084.62113