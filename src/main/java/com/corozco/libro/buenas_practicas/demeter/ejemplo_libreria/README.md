<h1 style="text-align:center;"> <strong>Ejemplo: librería</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se pidió a <em>nuestro desarrollador</em> crear un sistema que permita consultar ejemplares disponibles en una librería. El sistema debe mostrar al usuario la información básica de cada libro ---nombre, número de páginas, género, información del autor--- y facilitar la consulta del autor asociado a un libro específico.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar la Ley de Demeter</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/demeter/ley_demeter_ejemplo_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando la Ley de Demeter</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/demeter/ley_demeter_ejemplo_bien.png" width=70% height=70% alt="">
</p>