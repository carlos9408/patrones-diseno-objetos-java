package com.corozco.libro.buenas_practicas.dry.dry_documentacion;

/**
 * Esta clase se llama Calculadora.
 * Sirve para realizar operaciones matemáticas básicas.
 * + Se espera agregar nuevas operaciones en el futuro.
 */
public class Calculadora {

    /**
     * Este método suma dos números.
     *
     * @param a El primer número a sumar.
     * @param b El segundo número a sumar.
     * @return La suma de los dos números.
     */
    public int sumarDosNumeros(int a, int b) {
        return a + b; // Suma los dos números y devuelve el resultado
    }
}