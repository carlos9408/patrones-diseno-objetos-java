package com.corozco.libro.buenas_practicas.dry.dry_logica.entidades;

public class Profesor {
    private String documento;
    private String nombre;
    private String correoElectronico;
    private String departamento;
    private String tipoContrato;
    private int horasAsignadas;
}
