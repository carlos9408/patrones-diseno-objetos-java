package com.corozco.libro.buenas_practicas.dry.dry_logica.entidades;

public class Estudiante {
    private String documento;
    private String nombre;
    private String correoElectronico;
    private double promedio;
    private int semestre;
}
