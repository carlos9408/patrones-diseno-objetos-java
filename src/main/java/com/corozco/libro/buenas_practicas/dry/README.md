<h1 style="text-align:center;">Principio DRY (Don’t Repeat Yourself)</h1>

<hr>

<h4 style="text-align:center;"><em>"Cada pieza de conocimiento debe tener una representación única, clara y autoritativa
dentro de un sistema."</em></h4>

<p style="text-align:justify;">
    El principio <strong>DRY</strong>, cuyo acrónimo significa <em>“Don’t Repeat Yourself”</em> ---traducido al español como <em>“No te repitas”</em>---, es un principio de desarrollo de software que fue introducido formalmente en el año 1999 en el libro <em>The Pragmatic Programmer</em> (<a href="#Hunt y Thomas">Hunt y Thomas, 1999</a>).
</p>

<p style="text-align:justify;">
    Durante los primeros años en que se difundió y aplicó este principio, la comunidad de desarrollo de software asumió que se enfocaba en evitar la duplicación de código. Sin embargo, con el paso de los años, los autores consideraron pertinente aclarar que <strong>DRY</strong> se aplica a toda forma de conocimiento dentro de un sistema, por ejemplo: documentación, reglas de negocio, datos, entre otros.
</p>

<hr>

<h2>Consideraciones</h2>

<p style="text-align:justify;">
    Aplicar el principio <strong>DRY</strong> puede resultar complejo para las personas que tienen poca experiencia en el diseño de software. A continuación, se presentan algunos lineamientos o condiciones que se pueden considerar para identificar si una solución se puede mejorar a través de este principio:
</p>

<ul style="text-align:justify;">

<li><strong>Identificación de duplicación:</strong> Debe existir una duplicación clara de lógica, datos o estructuras dentro del sistema. Esta duplicación se puede presentar en cualquier artefacto utilizado durante el ciclo de vida del desarrollo de una solución, por ejemplo: documentación, código fuente, pruebas automatizadas, entornos de configuración, entre otros.</li>

<li><strong>Contexto común:</strong> Los elementos funcionales identificados como duplicados a nivel técnico deben responder al mismo propósito o contexto. Si dos elementos duplicados resuelven problemas conceptualmente diferentes, no se pueden agrupar, ya que los requisitos y objetivos específicos de cada contexto pueden variar.</li>

<li><strong>Posibilidad de abstracción:</strong> Debe ser posible abstraer la lógica o datos duplicados en una estructura de alto nivel, por ejemplo: una función, clase, módulo o documento. La abstracción debe capturar adecuadamente la funcionalidad común garantizando que su aplicación será agnóstica entre diferentes contextos.</li>

<li><strong>Mantenibilidad mejorada:</strong> El resultado después de aplicar el principio debe facilitar la lectura y extensión de la solución en el futuro.</li>

<li><strong>Beneficio claro sobre los costos de refactorización:</strong> Aplicar este principio debe tener un beneficio claro y tangible que justifique los costos, recursos y tiempo necesarios para llevar a cabo tareas de refactorización. Si los costos son mayores que los beneficios potenciales, se sugiere no aplicar el principio.</li>

</ul>

<hr>

<h2>Referencias</h2>

* Hunt, A., & Thomas, D. (1999). The pragmatic programmer: From journeyman to master. Addison-Wesley Professional. ISBN
  978-0-2016-1622-4.