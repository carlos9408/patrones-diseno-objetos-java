package com.corozco.libro.buenas_practicas.dry.dry_documentacion;

// Utilidad que permite realizar operaciones aritméticas simples
public class CalculadoraDRY {
    public int sumar(int a, int b) {
        return a + b;
    }
}