package com.corozco.libro.buenas_practicas.solid.liskov.ejemplo2_estacion_servicio.aplicando_principio;


import lombok.Data;

@Data
public class Solicitud {

    private double porcentajeGasolina;
    private double porcentajeRecarga;

}
