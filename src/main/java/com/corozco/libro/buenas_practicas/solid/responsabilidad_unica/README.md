<h1 style="text-align:center;">S: Single Responsibility Principle (SRP)</h1>

<hr>

<h4 style="text-align:center;"><em>"Una clase debería tener solo una razón para cambiar"</em></h4>

<p style="text-align:justify;">
    El <strong>Principio de Responsabilidad Única</strong> establece que una clase debe tener una, y solo una, razón para cambiar; es decir, cada clase debe encargarse exclusivamente de una parte de la funcionalidad del software, y dicha responsabilidad debe estar encapsulada dentro de la clase. En el contexto de este principio, una responsabilidad se define como <em>una razón para cambiar</em>.
</p>

<p style="text-align:justify;">
    Desde el punto de vista histórico, el <strong>Principio de Responsabilidad Única</strong> se puede interpretar como una evolución del principio de <strong>alta cohesión</strong> sugerido en <strong>GRASP</strong>.
</p>

<hr>

<h2>Analogía: sujeto multidisciplinar</h2>

<p style="text-align:justify;">
    Para entender este principio de manera intuitiva, considera la siguiente analogía: <strong>José</strong> ha pasado una gran parte de su vida formándose en diferentes campos. Como resultado, ahora es capaz de desempeñarse en múltiples disciplinas con relativa facilidad.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/responsabilidad_unica/srp_analogia_caso1.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Inicialmente, se podría pensar que tener a una persona capaz de asumir múltiples responsabilidades es una ventaja. Sin embargo, <strong>José</strong> es incapaz de apoyar a sus compañeros la mayor parte del tiempo porque su papel como recurso centralizado lo convierte en un cuello de botella para sus colegas. El entorno de <strong>José</strong> no cumple el principio de responsabilidad única porque él asume demasiadas responsabilidades, afectando a sus compañeros.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/responsabilidad_unica/srp_analogia_caso2.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Para no depender siempre de <strong>José</strong>, sus compañeros decidieron buscar a otra persona que pudiera atender sus necesidades específicas. A diferencia del caso anterior, el entorno de <strong>José</strong> ahora cumple con el principio de responsabilidad única, ya que cada compañero encontró un especialista para cada tarea, sin afectar las solicitudes de los demás.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/responsabilidad_unica/srp_analogia_caso3.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Volviendo al contexto de la ingeniería de software, el principio de responsabilidad única tiene como objetivo asegurar que cada parte del sistema se enfoque en un aspecto específico del problema a resolver. Dividir las clases o módulos en unidades independientes que aborden necesidades concretas fomenta la creación de software con un alto nivel de cohesión.
</p>