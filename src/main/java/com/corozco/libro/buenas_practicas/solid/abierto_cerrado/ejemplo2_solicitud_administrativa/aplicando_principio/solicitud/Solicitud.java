package com.corozco.libro.buenas_practicas.solid.abierto_cerrado.ejemplo2_solicitud_administrativa.aplicando_principio.solicitud;

public interface Solicitud {
    void ejecutar();
}
