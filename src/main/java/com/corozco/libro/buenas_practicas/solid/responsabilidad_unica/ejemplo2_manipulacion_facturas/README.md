<h1 style="text-align:center;"> <strong>Ejemplo práctico: manipulación de facturas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a <em>nuestro desarrollador</em> crear un sistema que permita exportar e imprimir facturas. Una factura se puede exportar en tres formatos (pdf, texto plano y xls).
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Responsabilidad Única</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/responsabilidad_unica/srp_ejemplo2_mal.png" width="2320" height="auto" alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Responsabilidad Única</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/responsabilidad_unica/srp_ejemplo2_bien.png" width="2605" height="auto" alt="">
</p>