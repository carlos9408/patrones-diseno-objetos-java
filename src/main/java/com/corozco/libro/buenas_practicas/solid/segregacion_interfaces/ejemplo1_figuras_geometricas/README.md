<h1 style="text-align:center;"> <strong>Ejemplo abstracto: figuras geométricas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a <em>nuestro desarrollador</em> implementar una solución que permita calcular las propiedades básicas de diferentes figuras geométricas, por ejemplo: su longitud, área y volumen.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Segregación de Interfaces</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/segregacion_interfaces/isp_ejemplo1_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Segregación de Interfaces</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/segregacion_interfaces/isp_ejemplo1_bien.png" width=70% height=70% alt="">
</p>