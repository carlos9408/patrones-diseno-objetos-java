package com.corozco.libro.buenas_practicas.solid.inversion_dependencias.ejemplo2_servicio_almacenamiento.sin_aplicar_principio.servicios;

public class AlmacenamientoNube {

    public void guardar(String info) {
        System.out.println("Guarda en la nube");
    }
}
