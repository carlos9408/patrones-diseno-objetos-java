package com.corozco.libro.buenas_practicas.solid.abierto_cerrado.ejemplo1_figuras_geometricas.aplicando_principio.figura;

public abstract class Figura {

    public abstract double getArea();
}
