package com.corozco.libro.buenas_practicas.solid.abierto_cerrado.ejemplo2_solicitud_administrativa.sin_aplicar_principio;

public enum TipoSolicitud {
    HOMOLOGACION, CANCELACION, VALIDACION
}
