<h1 style="text-align:center;"> <strong>Ejemplo practico: servicio de almacenamiento</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se ha solicitado a <em>nuestro desarrollador</em>  crear una solución que permita persistir información mediante diferentes mecanismos, por ejemplo: persistencia en disco duro, en una base de datos o en un sistema de almacenamiento en la nube.
</p>



<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Inversión de Dependencias</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/inversion_dependencias/dip_ejemplo2_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Inversión de Dependencias</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/inversion_dependencias/dip_ejemplo2_bien.png" width=70% height=70% alt="">
</p>