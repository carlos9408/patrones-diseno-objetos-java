<h1 style="text-align:center;"> <strong>Ejemplo práctico: gestión de dispositivos IoT</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a <em>nuestro desarrollador</em> crear un sistema que facilite la gestión de diferentes dispositivos IoT, por ejemplo: sensores de temperatura y cámaras de seguridad. Por diseño, cada dispositivo proporciona distintas capacidades y mecanismos de control.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Segregación de Interfaces</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/segregacion_interfaces/isp_ejemplo2_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Segregación de Interfaces</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/segregacion_interfaces/isp_ejemplo2_bien.png" width=70% height=70% alt="">
</p>