<h1 style="text-align:center;">L: Liskov Substitution Principle (LSP)</h1>

<hr>

<h4 style="text-align:center;"><em>"Los subtipos de una clase deben ser sustituibles por su superclase"</em></h4>

<p style="text-align:justify;">
    El <strong>Principio de sustitución Liskov</strong> fue formulado en el año 1987 por Barbara Liskov, quien introdujo el término por primera vez durante una presentación magistral titulada: <em>Data Abstraction and Hierarchy</em> (Liskov, 1987). De acuerdo con Barbara Liskov, dada una jerarquía conformada por una superclase y un conjunto de subclases, el sistema debe ser capaz de permitir que las subclases reemplacen a la superclase sin afectar el comportamiento normal del sistema.
</p>

<hr>

<h2>Analogía - cafetería</h2>

<p style="text-align:justify;">
    Para entender este principio de manera intuitiva, considera la siguiente analogía: <strong>Julián</strong> es dueño de una cafetería y sabe cómo preparar todas las bebidas que ofrece el establecimiento.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/liskov/lsp_analogia_caso1.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    <strong>Julián</strong> tiene dos hijos: <strong>Andrés</strong> e <strong>Isabel</strong>, quienes ayudan a su papá a atender la cafetería cuando él no está. <strong>Andrés</strong> disfruta pasar tiempo con su padre y siempre busca una excusa para ayudar en la cafetería, por lo que se ha vuelto experto en el negocio. <strong>Andrés</strong> cumple el principio de sustitución de Liskov porque puede reemplazar a su padre sin afectar el negocio.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/liskov/lsp_analogia_caso2.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Por otro lado, <strong>Isabel</strong> no pasa mucho tiempo en la cafetería porque prefiere dedicarse a otras actividades. Como resultado, su padre no le ha enseñado a preparar bebidas elaboradas — batidos, infusiones y nevados. De esta manera, se observa que <strong>Isabel</strong> no cumple el principio de sustitución de Liskov porque no puede reemplazar a su padre, afectando las ventas cuando él no está.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/liskov/lsp_analogia_caso3.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Volviendo al contexto de la ingeniería de software, el principio de sustitución de Liskov busca que las jerarquías de clases tengan sentido a nivel estructural y conceptual. Que una subclase no sea capaz de reemplazar a su superclase es un síntoma de una abstracción con defectos de diseño.
</p>