<h1 style="text-align:center;">Principios SOLID</h1>

<hr>

<p style="text-align:justify;">
    Los principios <strong>SOLID</strong> son un conjunto de buenas prácticas para el diseño de software propuestas por Robert C. Martin entre las décadas de 1980 y 2000. Estos principios surgieron gradualmente como resultado del trabajo de Martin, que incluye artículos científicos, libros, publicaciones y presentaciones. Sin embargo, el acrónimo <strong>SOLID</strong> fue introducido por Michael Feathers, uno de los pioneros en el análisis orientado a objetos y autor del libro <em>Working Effectively with Legacy Code</em> (<a href="#Feathers">Feathers, 2004</a>). Feathers buscó agrupar el conocimiento generado por Martin y otros autores influyentes como Barbara Liskov, Bertrand Meyer y David Parnas en un concepto fácil de recordar y aplicar. Con el tiempo, el acrónimo <strong>SOLID</strong> se ha establecido como un referente para el desarrollo de soluciones orientadas a objetos, promoviendo el diseño e implementación de código limpio.
</p>

<p style="text-align:justify;">
    Cada uno de los principios <strong>SOLID</strong> busca mejorar la estructura, consistencia y mantenimiento del código, promoviendo la creación de software robusto, flexible y escalable. Gracias a su naturaleza agnóstica, estos principios han demostrado ser efectivos para resolver problemas alineados con el paradigma orientado a objetos, promoviendo la implementación de soluciones con niveles óptimos de cohesión y acoplamiento. A continuación, se presenta el significado de cada una de las letras que conforman el acrónimo <strong>SOLID</strong>:
</p>

<ul style="text-align:justify; list-style-type: disc;">
    <li><strong>S:</strong> Single Responsibility Principle (SRP)</li>
    <li><strong>O:</strong> Open/Closed Principle (OCP)</li>
    <li><strong>L:</strong> Liskov Substitution Principle (LSP)</li>
    <li><strong>I:</strong> Interface Segregation Principle (ISP)</li>
    <li><strong>D:</strong> Dependency Inversion Principle (DIP)</li>
</ul>

<p style="text-align:justify;">
    <strong>Nota:</strong> A medida el lector avance en los ejemplos, notará que los principios <strong>SOLID</strong> son una evolución de los principios <strong>GRASP</strong>, adaptados a los desafíos de la industria del software contemporánea. En otras palabras, los principios <strong>SOLID</strong> son los principios <strong>GRASP</strong> potenciados para enfrentar los retos actuales.
</p>

<hr>

<h2>Referencias</h2>

* Feathers, M. (2004). Working effectively with legacy code. Prentice Hall.