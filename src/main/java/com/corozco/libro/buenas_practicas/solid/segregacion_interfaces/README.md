<h1 style="text-align:center;">I: Interface Segregation Principle (ISP)</h1>

<hr>

<h4 style="text-align:center;"><em>"Los clientes no deben estar forzados a depender de métodos que no van a
usar"</em></h4>

<p style="text-align:justify;">
    El <strong>Principio de Segregación de Interfaces</strong> establece que una clase no debe estar obligada a implementar métodos que no necesita. Este principio promueve la creación de interfaces específicas y pequeñas, en lugar de interfaces generales que obliguen a las clases a implementar métodos innecesarios desde el punto de vista de la solución.
</p>

<hr>

<h2>Analogía: cita</h2>

<p style="text-align:justify;">
    Para entender este principio de manera intuitiva, considera la siguiente analogía: <strong>Carlos</strong> invitó a <strong>Isabel</strong> a cenar en un nuevo restaurante/bar que recientemente abrió en la ciudad. Luego de revisar su agenda, <strong>Isabel</strong> aceptó la invitación con entusiasmo.
</p>

<p style="text-align:justify;">
    Con el paso de los días, surgieron compromisos inesperados que llevaron a <strong>Carlos</strong> e <strong>Isabel</strong> a asistir a cenas de carácter laboral y social, respectivamente, justo en el horario propuesto para su cita. Sin embargo, decididos a no posponer el encuentro, ambos acordaron sustituir la cena por una charla acompañada de cócteles.
</p>

<p style="text-align:justify;">
    Cuando llegó el día de la cita, <strong>Carlos</strong> llevó a <strong>Isabel</strong> al restaurante, donde fueron recibidos de manera cordial por <strong>Leonardo</strong>, su mesero durante la velada. Después de encontrar un lugar cómodo, <strong>Carlos</strong> solicitó la carta de cócteles a <strong>Leonardo</strong>, quien le explicó que el restaurante utiliza un menú general que abarca todo lo que ofrece el establecimiento: entradas, aperitivos, bebidas sin alcohol, bebidas alcohólicas, comida rápida, platos fuertes, postres, entre otros. Debido a la amplia variedad de opciones, <strong>Carlos</strong> e <strong>Isabel</strong> pasaron varios minutos buscando las bebidas que querían pedir.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/segregacion_interfaces/isp_analogia_caso1.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    La cita se llevó a cabo sin inconvenientes y fue un encuentro agradable para ambos. Como resultado, <strong>Carlos</strong> e <strong>Isabel</strong> continuaron saliendo durante un tiempo, hasta que finalmente decidieron formalizar su relación. Luego de un año, <strong>Carlos</strong> invitó a <strong>Isabel</strong> nuevamente al mismo restaurante donde tuvieron su primera cita para celebrar su primer aniversario. A diferencia del año anterior, esta vez <strong>Carlos</strong> realizó una reserva para dos personas y ambos se aseguraron de no aceptar otros planes.
</p>

<p style="text-align:justify;">
    Al llegar al restaurante, fueron recibidos por <strong>Lucía</strong>, quien verificó la reserva, los condujo a su mesa y les informó que el restaurante ahora ofrece varias cartas. Gracias a esta mejora en la experiencia del cliente, <strong>Carlos</strong> e <strong>Isabel</strong> pueden concentrarse en revisar sus pedidos específicos sin tener que revisar un menú extenso.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/segregacion_interfaces/isp_analogia_caso2.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Volviendo al contexto de la ingeniería de software, el principio de segregación de interfaces busca evitar que las clases dependan de métodos innecesarios al dividir interfaces grandes en varias más específicas. Esto promueve la creación de sistemas modulares y flexibles, donde cada clase o parte del sistema se enfoca únicamente en las funcionalidades que realmente necesita.
</p>