<h1 style="text-align:center;">O: Open/Closed Principle (OCP)</h1>

<hr>

<h4 style="text-align:center;"><em>"Las entidades de software (clases, módulos o funciones) deben estar abiertas para la
extensión, pero cerradas para la modificación"</em></h4>

<p style="text-align:justify;">
    El <strong>Principio Abierto/Cerrado</strong> establece que un sistema debe ser capaz de agregar nuevas funcionalidades sin afectar la lógica existente. En otras palabras, una clase o módulo puede aceptar cambios solo si estos no implican modificar el código fuente que ya está funcionando en entornos productivos.
</p>

<p style="text-align:justify;">
    Para dar un contexto más completo sobre este principio al lector, es necesario explicar los conceptos de <strong>abierto</strong> y <strong>cerrado</strong>:
</p>

<ul style="text-align:justify; list-style-type: disc;">
    <li><strong>Abierto:</strong> Se refiere a la capacidad que tiene una entidad — como una clase o un módulo — de añadir funcionalidades o comportamientos mediante la adición de nuevo código. Esto se puede lograr utilizando técnicas de composición y generalización.</li>
    <li><strong>Cerrado:</strong> Una entidad se considera cerrada cuando se prohíbe realizar cambios en su código fuente. Por estándar, esto sucede cuando la entidad está lista para pasar a entornos productivos. Cerrar una entidad previene errores debidos a cambios futuros y garantiza que las funcionalidades existentes se mantengan intactas.</li>
</ul>

<hr>

<h2>Analogía - máquina expendedora</h2>

<p style="text-align:justify;">
    Para entender este principio de manera intuitiva, considera la siguiente analogía: <strong>Manuel</strong> administra una tienda que dispone de una máquina de gancho activada por monedas, donde los clientes pueden intentar ganar un juguete. Luego de un tiempo, <strong>Manuel</strong> notó que la máquina es bastante popular entre los niños, así que le pidió a <strong>Juan</strong> —su operario de confianza— que agregue nuevos juguetes a la máquina para atraer más clientes. Agregar nuevos juguetes cumple con el principio abierto/cerrado porque no afecta el funcionamiento de la máquina.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/abierto_cerrado/ocp_analogia_caso1.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Además, <strong>Manuel</strong> se dio cuenta de que la mayoría de los niños acostumbran a pedir dinero a sus padres para comprar golosinas. Esto llevó a <strong>Manuel</strong> a considerar la idea de modificar la máquina de gancho para que también sirva como un dispensador de golosinas. Afortunadamente, <strong>Juan</strong> le advirtió que hacerlo puede ser contraproducente, ya que implica gastar recursos modificando una máquina que ya funciona. Modificar la máquina de gancho incumple el principio abierto/cerrado porque busca cambiar una estructura que en principio es inmutable.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/abierto_cerrado/ocp_analogia_caso2.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    <strong>Juan</strong> —en su labor de buen operario— le recomendó a <strong>Manuel</strong> instalar una máquina dispensadora de golosinas al lado de la máquina de gancho. Instalar una nueva máquina cumple el principio abierto/cerrado porque no afecta la infraestructura original.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/abierto_cerrado/ocp_analogia_caso3.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Volviendo al contexto de la ingeniería de software, un aspecto interesante del principio de abierto/cerrado es que su impacto suele hacerse evidente —en la mayoría de los casos— cuando ya se ha acumulado deuda técnica en el proyecto. Esto ocurre porque las decisiones de diseño pueden cambiar con el tiempo debido a diversos factores, por ejemplo: abstracciones parciales, malas decisiones iniciales, o cambios no previstos en las etapas tempranas del proyecto.
</p>

<p style="text-align:justify;">
    El principio abierto/cerrado es relativamente fácil de entender. Sin embargo, aplicarlo puede representar un desafío significativo, ya que los proyectos de software cambian y evolucionan con el tiempo, resultando en ajustes que impactan decisiones de diseño previas.
</p>