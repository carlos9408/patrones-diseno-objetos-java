<h1 style="text-align:center;">D: Dependency Inversion Principle (DIP)</h1>

<hr>

<h4 style="text-align:center;"><em>"Las abstracciones no deben depender de los detalles. Los detalles deben depender de
las abstracciones."</em></h4>

<p style="text-align:justify;">
    El <strong>Principio de Inversión de Dependencias</strong> establece que las dependencias en un sistema deben estar orientadas hacia interfaces o abstracciones en lugar de implementaciones concretas. El propósito de este principio es reducir la dependencia entre partes funcionales de un sistema, asegurando que la comunicación entre módulos se realiza a través de clases de alto nivel. Como resultado, las implementaciones concretas se pueden cambiar o reemplazar sin afectar la estructura general del sistema.
</p>

<hr>

<h2>Analogía: orquesta</h2>

<p style="text-align:justify;">
    Para entender este principio de manera intuitiva, considera la siguiente analogía: <strong>Camilo</strong> es el director de una orquesta sinfónica que se presenta regularmente en teatros de la ciudad. Antes de cada concierto, <strong>Camilo</strong> da instrucciones específicas a cada músico para asegurar que todos los músicos estén sincronizados.
</p>

<p style="text-align:justify;">
    La estrategia aplicada por <strong>Camilo</strong> fue efectiva cuando la banda era pequeña. Sin embargo, debido al éxito en sus presentaciones, fue necesario incluir a nuevos músicos. Con el paso del tiempo, <strong>Camilo</strong> notó que pasaba mucho tiempo adaptando sus sugerencias e instrucciones a todos los miembros de la orquesta.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/inversion_dependencias/dip_analogia_caso1.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Luego de pensar un poco, <strong>Camilo</strong> notó que gran parte de su tiempo lo dedicaba a explicar a cada músico la partitura que debía interpretar en los conciertos. Con esto en mente, <strong>Camilo</strong> encargó a <strong>Javier</strong> ---su asistente--- la tarea de distribuir partituras adaptadas a cada músico.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/inversion_dependencias/dip_analogia_caso2.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Gracias a esta nueva estrategia, <strong>Camilo</strong> solo debe preocuparse por conocer la partitura general y entregarla a <strong>Javier</strong> para que la adapte a la necesidad de cada músico. Por otro lado, cualquier nuevo músico que se una al equipo solo necesita familiarizarse con la partitura correspondiente a su instrumento.
</p>

<p style="text-align:center;">
    <img src="../../../../../../../resources/images/principios/solid/inversion_dependencias/dip_analogia_caso3.png" width="100%" height="100%" alt="">
</p>

<p style="text-align:justify;">
    Volviendo al contexto de la ingeniería de software, el principio de Inversión de Dependencias asegura que los componentes del sistema sean intercambiables y fáciles de modificar o extender. Al depender de abstracciones, un sistema puede adaptarse a los cambios en la lógica del negocio sin requerir modificaciones en los componentes de alto nivel.
</p>