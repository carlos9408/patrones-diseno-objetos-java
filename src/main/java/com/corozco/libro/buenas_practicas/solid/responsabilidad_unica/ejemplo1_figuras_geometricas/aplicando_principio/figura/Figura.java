package com.corozco.libro.buenas_practicas.solid.responsabilidad_unica.ejemplo1_figuras_geometricas.aplicando_principio.figura;

public interface Figura {
    double getPerimetro();
}
