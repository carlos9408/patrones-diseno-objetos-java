<h1 style="text-align:center;"> <strong>Ejemplo práctico: estación de servicio</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se pidió a <em>nuestro desarrollador</em> implementar una solución que facilite el abastecimiento de combustible o suministro eléctrico a los vehículos que atiende una estación de servicio. Actualmente, la estación puede abastecer suministro a vehículos de combustión (funcionan con gasolina), eléctricos e híbridos.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Sustitución de Liskov</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/liskov/lsp_ejemplo2_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Sustitución de Liskov</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/liskov/lsp_ejemplo2_bien.png" width=70% height=70% alt="">
</p>