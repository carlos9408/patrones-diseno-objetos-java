package com.corozco.libro.buenas_practicas.solid.inversion_dependencias.ejemplo2_servicio_almacenamiento.aplicando_principio;

public interface ServicioAlmacenamiento {
    void guardar(String info);
}
