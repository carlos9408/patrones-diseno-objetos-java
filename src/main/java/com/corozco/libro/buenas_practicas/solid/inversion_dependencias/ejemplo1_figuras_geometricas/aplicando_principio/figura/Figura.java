package com.corozco.libro.buenas_practicas.solid.inversion_dependencias.ejemplo1_figuras_geometricas.aplicando_principio.figura;

public abstract class Figura {
    public abstract double getArea();
}
