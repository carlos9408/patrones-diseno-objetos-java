package com.corozco.libro.buenas_practicas.solid.responsabilidad_unica.ejemplo1_figuras_geometricas.sin_aplicar_principio;

public enum TipoFigura {
    CIRCULO, RECTANGULO, TRIANGULO
}
