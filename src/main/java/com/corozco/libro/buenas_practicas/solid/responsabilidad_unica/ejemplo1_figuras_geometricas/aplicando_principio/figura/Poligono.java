package com.corozco.libro.buenas_practicas.solid.responsabilidad_unica.ejemplo1_figuras_geometricas.aplicando_principio.figura;

public interface Poligono extends Figura {

    double getPerimetro();

    double getSumaAngulos();

    double getDiagonal();
}
