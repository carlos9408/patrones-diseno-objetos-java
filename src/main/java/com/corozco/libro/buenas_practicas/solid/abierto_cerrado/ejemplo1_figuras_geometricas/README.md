<h1 style="text-align:center;"> <strong>Ejemplo abstracto: figuras geométricas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se pidió a <em>nuestro desarrollador</em>  crear un programa que permita calcular el área de diferentes figuras geométricas.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio Abierto/Cerrado</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/abierto_cerrado/ocp_ejemplo1_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio Abierto/Cerrado</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/abierto_cerrado/ocp_ejemplo1_bien.png" width=70% height=70% alt="">
</p>