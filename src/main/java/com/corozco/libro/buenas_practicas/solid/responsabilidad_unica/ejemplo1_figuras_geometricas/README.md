<h1 style="text-align:center;"> <strong>Ejemplo abstracto: figuras geométricas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a <em>nuestro desarrollador</em> crear un sistema que permita calcular las propiedades de diferentes figuras geométricas, por ejemplo: su perímetro, suma de sus ángulos interiores, longitud de su diagonal, entre otras. Inicialmente, la solución debe soportar operaciones para rectángulos y círculos; sin embargo, se espera añadir nuevas figuras y propiedades en el futuro.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Responsabilidad Única</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/responsabilidad_unica/srp_ejemplo1_mal.png" width="2320" height="auto" alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Responsabilidad Única</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/responsabilidad_unica/srp_ejemplo1_bien.png" width="2605" height="auto" alt="">
</p>