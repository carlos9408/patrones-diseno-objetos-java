<h1 style="text-align:center;"> <strong>Ejemplo práctico: solicitudes administrativas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se ha solicitado a <em>nuestro desarrollador</em> sistematizar los procesos en una universidad. Su tarea específica es crear un módulo que permita a los estudiantes enviar diversas solicitudes (cancelación, homologación y validación de cursos) a la vicedecanatura, la cual se encargará de determinar si se aceptan o se rechazan.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio Abierto/Cerrado</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/abierto_cerrado/ocp_ejemplo2_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio Abierto/Cerrado</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/abierto_cerrado/ocp_ejemplo2_bien.png" width=70% height=70% alt="">
</p>