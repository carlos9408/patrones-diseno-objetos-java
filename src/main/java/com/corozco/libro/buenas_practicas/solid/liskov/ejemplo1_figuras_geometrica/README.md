<h1 style="text-align:center;"> <strong>Ejemplo abstracto: figuras geométricas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> En el estudio de la geometría se observa un comportamiento interesante: los cuadrados son considerados un caso particular de los rectángulos donde la base y la altura son iguales. <em>Nuestro desarrollador</em> decidió implementar un sistema que calcule el área de cuadrados y rectángulos para fines ilustrativos.
</p>


<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Sustitución de Liskov</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/liskov/lsp_ejemplo1_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Sustitución de Liskov</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/liskov/lsp_ejemplo1_bien.png" width=70% height=70% alt="">
</p>