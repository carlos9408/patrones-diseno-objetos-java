<h1 style="text-align:center;"> <strong>Ejemplo abstracto: figuras geométricas</strong></h1>

----

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a <em>nuestro desarrollador</em> implementar una solución que simule una fábrica para crear figuras geométricas. La fábrica tiene como propósito centralizar operaciones soportadas por diferentes figuras, por ejemplo: calcular el área total de todas las figuras en el sistema.
</p>


<h1 style="text-align:justify;"><strong>Modelo UML sin aplicar el Principio de Inversión de Dependencias</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/inversion_dependencias/dip_ejemplo1_mal.png" width=70% height=70% alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML aplicando el Principio de Inversión de Dependencias</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/solid/inversion_dependencias/dip_ejemplo1_bien.png" width=70% height=70% alt="">
</p>