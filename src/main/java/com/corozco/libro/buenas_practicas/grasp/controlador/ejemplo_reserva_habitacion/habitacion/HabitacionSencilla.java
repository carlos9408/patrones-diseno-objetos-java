package com.corozco.libro.buenas_practicas.grasp.controlador.ejemplo_reserva_habitacion.habitacion;

public class HabitacionSencilla extends Habitacion {

    public HabitacionSencilla(int numero, double precio) {
        super(numero, precio);
    }
}
