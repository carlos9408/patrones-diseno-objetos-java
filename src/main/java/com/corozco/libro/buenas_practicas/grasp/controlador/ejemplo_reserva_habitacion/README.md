<h1 style="text-align:center;"> <strong>Ejemplo: Reserva de habitaciones</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a <em>nuestro desarrollador</em> implementar un sistema que facilite la gestión de reservas en un hotel. La solución debe ser capaz de crear y eliminar reservas solicitadas por un usuario. Actualmente, el hotel cuenta con tres tipos de habitaciones: sencillas, premium y de lujo. Además, el cliente indicó que se espera agregar nuevos tipos de habitaciones en el futuro.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/controlador/controlador_ejemplo.png" width="100%" height="100%" alt="">
</p>

----