<h1 style="text-align:center;"> <strong>Ejemplo: Logs personalizados</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se ha solicitado a \textit{nuestro desarrollador} implementar una utilidad que permita imprimir logs personalizados. Para lograrlo, se debe desarrollar una librería que permita a cualquier servicio generar logs adaptados a su dominio de negocio específico.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/polimorfismo/polimorfismo_ejemplo.png" width="100%" height="100%" alt="">
</p>

----