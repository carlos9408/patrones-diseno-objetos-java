package com.corozco.libro.buenas_practicas.grasp.creador.ejemplo_composicion;

public class Motor {

    private int potencia;
    private String tipo;

    public Motor(int potencia, String tipo) {
        this.potencia = potencia;
        this.tipo = tipo;
    }
}
