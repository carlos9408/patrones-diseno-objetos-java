package com.corozco.libro.buenas_practicas.grasp.acoplamiento.ejemplo.ejemplo_bajo_acoplamiento.servicios;

public interface ServicioNotificacion {

    void enviarMensaje(final String mensaje);
}
