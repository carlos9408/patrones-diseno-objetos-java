package com.corozco.libro.buenas_practicas.grasp.polimorfismo.ejemplo.figura;

public interface Figura {

    double calcularArea();
}
