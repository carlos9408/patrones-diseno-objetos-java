package com.corozco.libro.buenas_practicas.grasp.variacion_protegida.ejemplo.metodo_pago;

public abstract class MetodoPago {

    public abstract void realizarPago(double monto);
}
