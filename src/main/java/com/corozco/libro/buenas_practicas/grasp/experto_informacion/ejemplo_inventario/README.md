<h1 style="text-align:center;"> <strong>Ejemplo: Registro de inventario</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se ha solicitado a <em>nuestro desarrollador</em> crear un sistema que permita calcular el inventario de productos disponibles en una tienda. El sistema deberá determinar tanto el costo unitario de cada producto como el costo total de todos los productos en el inventario. Además, deberá permitir la incorporación de nuevos lotes de productos conforme ingresen a la tienda.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/experto_informacion/experto_informacion_ejemplo.png" width="2320" height="auto" alt="">
</p>

----