<h1 style="text-align:center;"> <strong>Variaciones protegidas</strong></h1>

----

<p style="text-align:justify;">
El principio de <strong>variaciones protegidas</strong> se centra en identificar los puntos de variabilidad en el sistema y encapsularlos detrás de interfaces o abstracciones. Esto asegura que los cambios en una parte del sistema no afecten negativamente al resto de la aplicación. El objetivo de este principio es minimizar el impacto de los cambios, garantizando que las modificaciones en un componente no se propaguen a otros módulos o clases.
</p>

----

<h2><strong>Referencias</strong><br></h2>

* Stevens, W. P., Myers, G. J., & Constantine, L. L. (1974). Structured Design.
* Yourdon, E., & Constantine, L. L. (1979). Structured Design: Fundamentals of a Discipline of Computer Program and
  System Design.