<h1 style="text-align:center;"> <strong>Fabricación pura</strong></h1>

----

<p style="text-align:justify;"> 
    El principio de <strong>fabricación pura</strong> sugiere la creación de clases que no pertenecen al dominio del problema pero que son necesarias para soportar aspectos técnicos o de infraestructura del sistema, por ejemplo: control de logs, gestión de transacciones, envío de notificaciones, entre otros. Estas clases sirven para encapsular aspectos técnicos que fomentan el desarrollo de código limpio, desacoplado y cohesivo.
</p>


----