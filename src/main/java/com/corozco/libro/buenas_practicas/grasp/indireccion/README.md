<h1 style="text-align:center;"> <strong>Indirección</strong></h1>

----

<p style="text-align:justify;"> 
    El principio de <strong>indirección</strong> propone introducir un objeto intermedio para reducir el acoplamiento entre dos o más clases. Este objeto actúa como un mediador que facilita la comunicación entre las clases y permite que interactúen sin depender directamente unas de otras.
</p>


----