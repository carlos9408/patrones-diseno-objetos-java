package com.corozco.libro.buenas_practicas.grasp.acoplamiento.ejemplo.ejemplo_alto_acoplamiento;

public enum TipoServicio {
    EMAIL, SMS, PUSH
}
