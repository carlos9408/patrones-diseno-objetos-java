package com.corozco.libro.buenas_practicas.grasp.cohesion.ejemplo.ejemplo_alta_cohesion;

// Magia para realizar peticiones HTTP
public class UtilidadesHttp {
    public void enviarPeticionGet(String url) {
    }

    public void enviarPeticionPost(String url, String mensaje) {
    }
}
