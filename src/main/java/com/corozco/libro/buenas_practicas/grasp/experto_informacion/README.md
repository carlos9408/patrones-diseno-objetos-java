<h1 style="text-align:center;"> <strong>Experto en información</strong></h1>

----

<h2><strong>Definición</strong><br></h2>
<p style="text-align:justify;"> 
    El principio <strong>Experto en Información</strong> sugiere que la responsabilidad de realizar una tarea se debe asignar a la clase que tiene la información necesaria para llevarla a cabo. En otras palabras, la clase que contiene los datos necesarios para ejecutar una función debe ser la que la implemente.
</p>


----