<h1 style="text-align:center;"> <strong>GRASP: Patrones Generales de Asignación de Responsabilidades</strong></h1>

----

<p style="text-align:justify;"> 
El acrónimo <strong>GRASP</strong> se traduce al español como <strong>Patrones Generales de Asignación de Responsabilidades</strong> y reúne un total de 9 patrones o sugerencias propuestas por Craig Larman en su libro <em>Applying UML and Patterns</em>. Estos principios están diseñados para facilitar la asignación de responsabilidades entre las estructuras que componen un sistema. A continuación, se presenta cada uno de los principios propuestos por Larman:
</p>

<ul>
<li>Creador (Creator)</li>
<li>Experto en Información (Information Expert)</li>
<li>Bajo Acoplamiento (Low Coupling)</li>
<li>Controlador (Controller)</li>
<li>Alta Cohesión (High Cohesion)</li>
<li>Polimorfismo (Polymorphism)</li>
<li>Fabricación Pura (Pure Fabrication)</li>
<li>Indirección (Indirection)</li>
<li>Variaciones Protegidas (Protected Variations)</li>
</ul>

<p style="text-align:justify;">
Cada principio está destinado a mejorar la claridad y mantenibilidad del diseño de software. En este proyecto, se comparten los ejemplos asociados con cada principio tal como se muestran en el libro.
</p>

----