package com.corozco.libro.buenas_practicas.grasp.cohesion.ejemplo.ejemplo_alta_cohesion;

import java.sql.Connection;

// Magia para gestión de conexiónarme a base de datos
public class UtilidadesDB {
    public void getConexion(String conexion) {
    }

    public void cerrarConexion(Connection conexion) {
    }
}
