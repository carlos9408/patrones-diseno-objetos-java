<h1 style="text-align:center;"> <strong>Ejemplo: Capa de control de acceso a datos</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se ha solicitado a \textit{nuestro desarrollador} crear un módulo genérico que permita guardar entidades (clases) de cualquier tipo en una base de datos. Por diseño, se espera que todas las entidades representadas en el sistema tengan un identificador.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/indireccion/indireccion_ejemplo.png" width="100%" height="100%" alt="">
</p>

----