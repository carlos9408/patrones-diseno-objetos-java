package com.corozco.libro.buenas_practicas.grasp.creador.ejemplo_agregacion;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Jugador {
    private String nombre;
    private int numero;
}