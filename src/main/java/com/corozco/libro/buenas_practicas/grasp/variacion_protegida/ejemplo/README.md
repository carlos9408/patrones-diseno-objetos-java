<h1 style="text-align:center;"> <strong>Ejemplo: Pasarela de pago</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a \textit{nuestro desarrollador} crear una solución que permita realizar pagos utilizando varios métodos ---tarjeta de crédito, pago por PayPal y pago seguro en linea (PSE)---. De acuerdo con el cliente, se espera que el sistema incluya nuevos tipos de pago en el futuro.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/variacion_protegida/variacion_protegida_ejemplo.png" width="100%" height="100%" alt="">
</p>

----