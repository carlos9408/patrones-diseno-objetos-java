<h1 style="text-align:center;"> <strong>Bajo Acoplamiento</strong></h1>

----

<h2><strong>Acoplamiento</strong><br></h2>


<p style="text-align:justify;">
El acoplamiento se refiere al grado de dependencia entre los componentes de un sistema; es decir, a cuán estrechamente conectados están y cómo el cambio en un componente puede afectar a los demás. Este principio busca fomentar la implementación de unidades independientes de software. Como resultado, se puede decir que una solución es buena si el grado de dependencia entre sus componentes es bajo.
</p>

En general, la dependencia se puede dividir en dos tipos:

- **Dependencia externa:** Ocurre cuando una operación dentro de un módulo necesita información, servicios o resultados
  de una operación en otro módulo para completarse.
- **Dependencia bidireccional:** Ocurre cuando dos módulos tienen operaciones que dependen mutuamente entre sí.

----

<h2><strong>Referencias</strong><br></h2>

* Stevens, W. P., Myers, G. J., & Constantine, L. L. (1974). Structured Design.
* Yourdon, E., & Constantine, L. L. (1979). Structured Design: Fundamentals of a Discipline of Computer Program and
  System Design.