package com.corozco.libro.buenas_practicas.grasp.controlador.ejemplo_reserva_habitacion.habitacion;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Habitacion {
    private int numero;
    private double precio;
}
