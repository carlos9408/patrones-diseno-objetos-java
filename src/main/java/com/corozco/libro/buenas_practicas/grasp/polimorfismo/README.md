<h1 style="text-align:center;"> <strong>Polimorfismo</strong></h1>

----

<p style="text-align:justify;">
El principio de <strong>polimorfismo</strong> sugiere aprovechar la flexibilidad proporcionada por la sobrecarga y sobrescritura de métodos para permitir que los objetos adopten comportamientos dinámicos en tiempo de ejecución. Este principio se basa en uno de los cuatro pilares básicos de la programación orientada a objetos que se explicó en el Capitulo 2.
</p>

----

<h2><strong>Referencias</strong><br></h2>

* Stevens, W. P., Myers, G. J., & Constantine, L. L. (1974). Structured Design.
* Yourdon, E., & Constantine, L. L. (1979). Structured Design: Fundamentals of a Discipline of Computer Program and
  System Design.