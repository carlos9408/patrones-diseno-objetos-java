<h1 style="text-align:center;"> <strong>Ejemplo: Utilidades</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> La empresa en la que trabaja <em>nuestro desarrollador</em> busca reducir la deuda técnica en uno de sus proyectos. Para lograrlo, se le ha encomendado la tarea de identificar puntos en la solución donde se pueda mejorar la cohesión entre sus componentes y después aplicar las acciones correctivas necesarias para mejorar el estado general del proyecto.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML (baja cohesión)</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/cohesion/baja_cohesion_ejemplo_clase.png" width="50%" height="50%" alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML (alta cohesión)</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/cohesion/alta_cohesion_ejemplo_clase.png" width="2320" height="auto" alt="">
</p>