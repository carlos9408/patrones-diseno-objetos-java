<h1 style="text-align:center;"> <strong>Ejemplo: Calcular el área de varias figuras geométricas</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se ha solicitado a <em>nuestro desarrollador</em> crear un programa que permita calcular el área de diferentes figuras geométricas.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/polimorfismo/polimorfismo_ejemplo.png" width="100%" height="100%" alt="">
</p>

----