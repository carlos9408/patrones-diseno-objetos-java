package com.corozco.libro.buenas_practicas.grasp.controlador.ejemplo_reserva_habitacion.habitacion;

public class HabitacionPremium extends Habitacion {

    public HabitacionPremium(int numero, double precio) {
        super(numero, precio);
    }
}
