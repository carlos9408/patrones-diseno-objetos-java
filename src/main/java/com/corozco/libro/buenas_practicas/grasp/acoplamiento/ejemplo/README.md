<h1 style="text-align:center;"> <strong>Ejemplo: Servicio de notificaciones</strong></h1>

----

<h1 style="text-align:justify;"><strong>Descripción del problema</strong><br></h1>

<p style="text-align:justify;">
<strong>Descripción del problema:</strong> Se solicitó a <em>nuestro desarrollador</em> implementar una solución que permita enviar alertas personalizadas. Inicialmente, el sistema debe estar diseñado para ser capaz de enviar mensajes por correo electrónico, mensajes SMS y alertas de tipo push. Sin embargo, el sistema debe ser capaz de agregar nuevos tipos de mensajes en el futuro.
</p>

<h1 style="text-align:justify;"><strong>Modelo UML (alto acoplamiento)</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/acoplamiento/alto_acoplamiento_ejemplo.png" width="100%" height="100%" alt="">
</p>

----

<h1 style="text-align:justify;"><strong>Modelo UML (bajo acoplamiento)</strong><br></h1>

<p style="text-align:center;">
    <img src="../../../../../../../../resources/images/principios/grasp/acoplamiento/bajo_acoplamiento_ejemplo.png" width="2320" height="auto" alt="">
</p>