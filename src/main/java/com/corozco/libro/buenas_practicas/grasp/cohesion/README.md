<h1 style="text-align:center;"> <strong>Alta Cohesión</strong></h1>

----

<h2><strong>Cohesión</strong><br></h2>

<p style="text-align:justify;">
La <strong>cohesión</strong> se refiere al grado en que los elementos dentro de una unidad de software están relacionados entre sí y trabajan juntos para cumplir una única tarea. Este principio busca asegurar que las funcionalidades dentro de una clase o módulo colaboren para cumplir un propósito común. Como resultado, se puede decir que una solución es "buena" si el grado de cohesión entre sus componentes es alta.
</p>

----

<h2><strong>Referencias</strong><br></h2>

* Stevens, W. P., Myers, G. J., & Constantine, L. L. (1974). Structured Design.
* Yourdon, E., & Constantine, L. L. (1979). Structured Design: Fundamentals of a Discipline of Computer Program and
  System Design.