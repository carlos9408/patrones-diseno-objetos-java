<h1 style="text-align:center;"> <strong>Controlador</strong></h1>

----

<p style="text-align:justify;">
El principio <strong>Controller</strong> sugiere que la responsabilidad de manejar eventos del sistema debería asignarse a una estructura o modelo específico llamado <strong>controlador</strong>, que actúa como intermediario entre la interfaz de usuario y el modelo del dominio de la aplicación. El objetivo de un controlador es centralizar la lógica de control en una clase específica para mejorar la modularidad y el mantenimiento del código. Esto contribuye a mantener un diseño de la aplicación más organizado, facilita el manejo de eventos o cambios, y promueve el desarrollo de componentes modulares con bajos niveles de acoplamiento.

</p>

----

<h2><strong>Referencias</strong><br></h2>

* Stevens, W. P., Myers, G. J., & Constantine, L. L. (1974). Structured Design.
* Yourdon, E., & Constantine, L. L. (1979). Structured Design: Fundamentals of a Discipline of Computer Program and
  System Design.