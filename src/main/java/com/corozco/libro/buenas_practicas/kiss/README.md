<h1 style="text-align:center;">Principio KISS (Keep It Simple, Stupid)</h1>

<hr>

<h4 style="text-align:center;"><em>"Entre varias soluciones posibles para un problema, siempre se debe elegir la más
simple".</em></h4>

<p style="text-align:justify;">
    El principio <strong>KISS</strong>, cuyo acrónimo significa <em>Keep It Simple, Stupid</em> ---traducido al español como <em>Mantenlo simple, estúpido</em>---, es una recomendación basada en la premisa de mantener la simplicidad en el diseño de cualquier solución.
</p>

<p style="text-align:justify;">
    La primera mención formal de este principio data del año 1938, cuando se publicó una entrada corta en el periódico Minneapolis Star con el título <em><em>Keep It Short and Simple</em></em>, en la que se explicaba la importancia de escribir documentos claros y concisos que fueran fáciles de entender para el público general (<a href="#Star">Star, 1938</a>). Sin embargo, el uso extendido del principio <strong>KISS</strong> como se conoce actualmente se remonta a la ingeniería militar de los años 60, particularmente en la marina de los Estados Unidos. Los ingenieros adoptaron esta filosofía para garantizar que los sistemas de armas fueran lo suficientemente simples como para ser reparados fácilmente bajo presión y en situaciones de combate. Con el tiempo, este principio fue adoptado en otras disciplinas, debido a su utilidad y aplicabilidad universal.
</p>

<p style="text-align:justify;">
    El principio <strong>KISS</strong> aplicado al contexto del desarrollo de software surgió como respuesta a la creciente complejidad de los sistemas informáticos a medida que avanzaban las tecnologías y las expectativas de los usuarios. La simplicidad se convirtió en un factor clave para asegurar que el software fuera escalable y sostenible a largo plazo.
</p>

<hr>

<h2>Referencias</h2>

* The Minneapolis Star. (1938, December 2). Keep It Short and Simple. Minneapolis, MN, USA, p. 20. Retrieved August 21,
  2022, from http://www.ioccc.org.